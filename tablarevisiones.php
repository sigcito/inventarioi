<?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();

    $sql = "SELECT r.id_revision, /* 0 */
    i.cod_imp,                    /* 1 */
    r.f_ejecucion,                /* 2 */
    o.nombre,                     /* 3 */
    t.contador,                   /* 4 */
    r.observaciones               /* 5 */FROM revisiones r LEFT JOIN impresora_fotocopiadora i ON r.id_impresora = i.cod_imp LEFT JOIN operadores o ON r.id_operador = o.id_operador LEFT JOIN toner_check t ON r.contador_impresora = t.contador WHERE r.id_revision";

    $result = mysqli_query($conexion,$sql);
?>

        <div>
            <table id="pagina" class="table table-hover custom-table">
                <thead class="thead-dark">
                    <tr class="">
                        <th scope="col">#</th>
                        <th scope="col">ID Impresora</th>
                        <th scope="col">Fecha revision</th>
                        <th scope="col">Operador</th>
                        <th scope="col">Contador</th>
                        <th scope="col">Observaciones</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while ($mostrar=mysqli_fetch_row($result)){

                          /*  $datos=$mostrar[0]."||".
                                    $mostrar[1]."||".
                                    $mostrar[2]."||".
                                    $mostrar[3]."||".
                                    $mostrar[4]."||".
                                    $mostrar[5]."||".
                                    $mostrar[6]."||".
                                    $mostrar[7]."||".
                                    $mostrar[8]."||".
                                    $mostrar[9]."||".
                                    $mostrar[10]."||".
                                    $mostrar[11]."||".
                                    $mostrar[12]."||".
                                    $mostrar[13]."||".
                                    $mostrar[14]."||".
                                    $mostrar[15];*/
                    ?>
                <tr>
                    <td><?php echo $mostrar[0] ?></td>
                    <td><?php echo $mostrar[1] ?></td>
                    <td><?php echo $mostrar[2] ?></td>
                    <td><?php echo $mostrar[3] ?></td>
                    <td><?php echo $mostrar[4] ?></td>
                    <td><?php echo $mostrar[5] ?></td>
                    <td style="text-align: center;">
                        <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modificar" onclick="agregaform('<?php echo $datos;?>')"> <i class="fas fa-pencil-alt fa-lg"></i></span>
                    </td>
                    <td style="text-align: center;">
                        <a class="btn btn-success" href="cartuchos_instalados.php">
                        <i class="fas fa-trash-alt fa-lg"></i></a>
                    </td>
                    <td style="text-align: center;">
                        <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#eliminar"><i class="fas fa-pencil-alt fa-lg"></i></span>
                    </td>
                </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function () {
	$('#pagina').DataTable({
		"language": {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
			"infoEmpty": "Mostrando 0 to 0 of 0 Datoss",
			"infoFiltered": "(Filtrado de _MAX_ total datos)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Datos",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		}
    });
    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4'
    });
});
</script>
