<?php 
require_once '../clases/conexion.php';

function getTecnico(){
    $obj = new conectar();
    $conexion = $obj->conexion();
    $id_empresa = $conexion->real_escape_string ($_POST['id_empresa']);
    $query = "SELECT * FROM tecnicos WHERE id_empresa = $id_empresa";
    $result = $conexion->query($query);
    $marcas = '<option value="">Elige una opción</option>';
    while($row = $result->fetch_array(MYSQLI_ASSOC)){
        $marcas .= '<option value="'."$row[id_tecnico]".'">'."$row[nombre]"." "."$row[apellido]".'</option>';
    }
    return $marcas;
}
echo getTecnico();
?>