<?php 
require_once '../clases/conexion.php';

function getTipoImp(){
    $obj = new conectar();
    $conexion = $obj->conexion();
    $query = "SELECT * FROM tipo_dispositivo";
    $result = $conexion->query($query);
    $listas = '<option value="0">Elige una opción</option>';
    while($row = $result->fetch_array(MYSQLI_ASSOC)){
        $listas .= '<option value="'."$row[id_dispositivo]".'">'."$row[des_tipo]".'</option>';
    }
    return $listas;
}
echo getTipoImp();
?>