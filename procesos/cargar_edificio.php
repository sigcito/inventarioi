<?php          
        require ('../clases/conexion.php');

        function getEdificioImp(){
            $obj = new conectar();
            $conexion = $obj->conexion();
            $query = "SELECT * FROM edificio";
            $result = $conexion->query($query);
            $edificio = '<option value="">Elige una opción</option>';
            while($row = $result->fetch_array(MYSQLI_ASSOC)){
                $edificio .= '<option value="'."$row[id_edificio]".'">'."$row[descripcion]".'</option>';
            }
            return $edificio;
        }
        echo getEdificioImp();
?>