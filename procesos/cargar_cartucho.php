<?php 
require_once '../clases/conexion.php';

function getMarcasImp(){
    $obj = new conectar();
    $conexion = $obj->conexion();
    $cod_toner = $conexion->real_escape_string ($_POST['cod_toner']);
    $query = "SELECT * FROM marca_toner WHERE id_marcatoner = $cod_toner";
    $result = $conexion->query($query);
    $marcas = '<option value="">Elige una opción</option>';
    while($row = $result->fetch_array(MYSQLI_ASSOC)){
        $marcas .= '<option value="'."$row[id_marcatoner]".'">'."$row[des_marcatoner]".'</option>';
    }
    return $marcas;
}
echo getMarcasImp();
?>