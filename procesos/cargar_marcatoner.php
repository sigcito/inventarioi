<?php 
require_once '../clases/conexion.php';

function getMarcaToner(){
    $obj = new conectar();
    $conexion = $obj->conexion();
    $query = "SELECT * FROM marca_toner";
    $result = $conexion->query($query);
    $marcas = '<option value="">Elige una opción</option>';
    while($row = $result->fetch_array(MYSQLI_ASSOC)){
        $marcas .= '<option value="'."$row[id_marca_toner]".'">'."$row[descripcion]".'</option>';
    }
    return $marcas;
}
echo getMarcaToner();
?>