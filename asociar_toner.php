<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
    <title>APP IMPRESORAS</title>
</head>

<body>
    <!--CABECERA-->
    <div class="container">
        <h1 class="text-center mb-5 mt-4">Cartuchos Instalados</h1>
    </div>
    <!--TABLA-->
    <div class="container-fluid">
        <div class="table-responsive">
            <?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();
    $impresoraid = $conexion->real_escape_string ($_GET ['datos']);
    $modelo = $conexion->real_escape_string ($_GET ['modelo']);

    $sql = "SELECT st.id_stock_toner,   /* 0 */
    mt.serial_modelo,                   /* 1 */
    st.serial_cartucho,                 /* 2 */
    mat.descripcion,                    /* 3 */
    mt.color,                           /* 4 */
    st.f_ingreso,                       /* 5 */
    st.observacion,                     /* 6 */
    id_status_condicion,                /* 7 */
    id_status_stock,                    /* 8 */
    st.cod_imp,                         /* 9 */
    st.cod_modelo,                      /* 10 */
    stat.des_status_toner               /* 11 */
    FROM stock_toner st LEFT JOIN modelo_toner mt ON mt.id_modelo_toner = st.cod_modelo LEFT JOIN marca_toner mat ON mat.id_marca_toner = mt.id_marca_toner LEFT JOIN status_toner stat ON stat.cod_statustoner = st.id_status_condicion WHERE st.cod_imp=$impresoraid AND id_status_stock=1";

    $result = mysqli_query($conexion,$sql);
?>
                <div>
                    <table id="paginatoner" class="table table-hover custom-table">
                        <thead class="thead-dark">
                                <tr class="">
                                <th scope="col">#</th>
                                <th scope="col">Serial</th>
                                <th scope="col">Serial cartucho</th>
                                <th scope="col">Marca</th>
                                <th scope="col">Color</th>
                                <th scope="col">Fecha Ingreso</th>
                                <th scope="col">Observacion</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                        </thead>
                        <tbody>
                            <?php
                        while ($mostrar=mysqli_fetch_row($result)){
                            $datostoners=$mostrar[0]."||".
                            $mostrar[1]."||".
                            $mostrar[2]."||".
                            $mostrar[3]."||".
                            $mostrar[4]."||".
                            $mostrar[5]."||".
                            $mostrar[6]."||".
                            $mostrar[7]."||".
                            $mostrar[8]."||".
                            $mostrar[9]."||".
                            $mostrar[10]."||".
                            $mostrar[11];
                    ?>
                                <tr>
                                    <td>
                                        <?php echo $mostrar[0];?>
                                    </td>
                                    <td>
                                        <?php echo $mostrar[1];?>
                                    </td>
                                    <td>
                                        <?php echo $mostrar[2];?>
                                    </td>
                                    <td>
                                        <?php echo $mostrar[3];?>
                                    </td>
                                    <td>
                                        <?php echo $mostrar[4];?>
                                    </td>
                                    <td>
                                        <?php echo $mostrar[5];?>
                                    </td>
                                    <td>
                                        <?php echo $mostrar[6];?>
                                    </td>
                                    <td>
                                        <?php echo $mostrar[11];?>
                                    </td>
                                    <td style="text-align: center;">
                                        <span class="btn btn-danger btn-sm" onclick="desocktoner('<?php echo $datostoners;?>')"><i class="fas fa-trash-alt fa-lg"></i></span>
                                    </td>
                                </tr>
                                <?php
                    }
                    ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tonerasoc">Asociar toner</button>
                    <a class="btn btn-secondary" href="index.php">Volver</a>
                </div>
        </div>
    </div>

    <div class="modal fade" id="tonerasoc" tabindex="-2" role="dialog" aria-labelledby="tonerasoc" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-center" id="exampleModalLabel"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
                </div>
                <div class="modal-body">
                    <form id="frmasociar_toner" class="needs-validation" novalidate method="get">
                    <h3 class="text-center mb-3">
                            Asociar toner
                        </h3>
                        <div class="form-row form-group mb-4">
                            <div class="col">
                                <label for="">Toner a asociar.</label>
                                <input type="text" id="tonerasustid" name="tonerasustid" hidden>
                                <input type="text" hidden="" id="active" name="active" value="1">
                                <input type="text" hidden="" id="impresoraid" name="impresoraid" value="<?php echo $impresoraid?>">
                                <input type="text" id="tonerasustd" name="tonerasustd" class="form-control" placeholder="Seleccione el toner a asociar en la tabla inferior"
                                disabled required="">
                                <br>
                                <label for="">Serial cartucho a asociar.</label>
                                <input type="text" id="serial_cartuchoStockU" name="serial_cartuchoStockU" class="form-control" placeholder="Ingrese el serial cartucho"
                                required="">
                            </div>
                        </div>
                        <hr>
                        <div class="container-fluid">
                            <h3 class="text-center" id="exampleModalLabel">Cargar toner nuevo.</h3>
                            <div class="table-responsive mb-4">
                                <?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();
    $impresoraid = $conexion->real_escape_string ($_GET ['datos']);
    $modelo = $conexion->real_escape_string ($_GET ['modelo']);

    $sql = "SELECT st.id_stock_toner,   /* 0 */
    mt.serial_modelo,                   /* 1 */
    st.serial_cartucho,                 /* 2 */
    mat.descripcion,                    /* 3 */
    mt.color,                           /* 4 */
    st.f_ingreso,                       /* 5 */
    st.observacion,                     /* 6 */
    id_status_condicion,                /* 7 */
    id_status_stock,                    /* 8 */
    st.cod_imp,                         /* 9 */
    st.cod_modelo,                      /* 10 */
    stat.des_status_toner               /* 11 */
    FROM stock_toner st LEFT JOIN modelo_toner mt ON mt.id_modelo_toner = st.cod_modelo LEFT JOIN marca_toner mat ON mat.id_marca_toner = mt.id_marca_toner LEFT JOIN status_toner stat ON stat.cod_statustoner = st.id_status_condicion LEFT JOIN compatibilidad c ON mt.id_modelo_toner = c.id_modelo_toner WHERE c.id_modelo_impresora=$modelo AND st.id_status_stock=0";

    $result = mysqli_query($conexion,$sql);
?>

                                    <div>
                                        <table id="toners" class="table table-hover custom-table" data-page-length="5">
                                            <thead class="thead-dark">
                                                <tr class="">
                                                    <th scope="col">Serial modelo</th>
                                                    <th scope="col">Serial cartucho</th>
                                                    <th scope="col">Color</th>
                                                    <th scope="col">Estado</th>
                                                    <th scope="col"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                        while ($mostrar=mysqli_fetch_row($result)){

                            $datosasoc=$mostrar[0]."||".
                                    $mostrar[1]."||".
                                    $mostrar[2]."||".
                                    $mostrar[3]."||".
                                    $mostrar[4]."||".
                                    $mostrar[5]."||".
                                    $mostrar[6]."||".
                                    $mostrar[7];
                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $mostrar[1] ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $mostrar[2] ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $mostrar[4] ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $mostrar[11] ?>
                                                        </td>
                                                        <td style="text-align: center;">
                                                            <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modificar" onclick="asoc_toner('<?php echo $datosasoc;?>')"> <i class="fas fa-pencil-alt fa-lg"></i></span>
                                                        </td>
                                                    </tr>
                                                    <?php
                    }
                    ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                            </div>
                            <div class="float-right">
        <button type="button" id="asociartoneraimpresora" class="btn btn-primary">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
    </div>
    </form>
                        </div>
                </div>
            </div>
        </div>
    </div>


</body>
<script src="librerias/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="librerias/alertify/alertify.js"></script>
<script src="librerias/fontawesome/svg-with-js/js/fontawesome-all.js"></script>
<script src="js/index.js"></script>
<script src="js/selecttoner.js"></script>

</html>

<script type="text/javascript">
    $(document).ready(function () {
        $('#paginatoner').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                "infoEmpty": "Mostrando 0 to 0 of 0 Datos",
                "infoFiltered": "(Filtrado de _MAX_ total datos)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Datos",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#toners').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                "infoEmpty": "Mostrando 0 to 0 of 0 Datos",
                "infoFiltered": "(Filtrado de _MAX_ total datos)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Datos",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    });
</script>
