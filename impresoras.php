<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
    <title>SalvaInv</title>
</head>

<body>
    <!--CABECERA-->
    <div class="container">
        <h1 class="text-center mb-5 mt-4">Listado de estaciones.</h1>
    </div>
    <div class="container">
        <nav class="navbar justify-content-between float-left">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#agregarImpresora">Agregar Impresora/Fotocopiadora</button>
            <button type="button" class="btn btn-primary ml-4" data-toggle="modal" data-target="#agregarcompatibilidad">Agregar compatibilidad</button>
            <a class="btn btn-primary ml-4" href="mantenimiento.php">Modulo mantenimiento.</a>
            <a class="btn btn-primary ml-4" href="toners.php">Ver listado de toners.</a>
            <a class="btn btn-primary ml-4" href="procesos/cerrar_sesion.php">Cerrar sesión.</a>
        </nav>
        <!--MODAL-->
        <div class="modal fade" id="agregarImpresora" tabindex="-1" role="dialog" aria-labelledby="agregarImpresora" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <!--TABS-->
                        <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#registrarImpresora" role="tab" aria-controls="pills-home"
                                    aria-selected="true">Registrar Impresora</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#agregarMarca" role="tab" aria-controls="pills-profile"
                                    aria-selected="false">Agregar Marca</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#agregarModelo" role="tab" aria-controls="pills-contact"
                                    aria-selected="false">Agregar Modelo</a>
                            </li>
                        </ul>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="registrarImpresora" role="tabpanel" aria-labelledby="registrarImpresora-tab">
                                <!--TAB REGISTRAR IMPRESORA-->
                                <div class="container">
                                    <h5 class="text-center">Agregar Detalles de Impresora</h5>
                                    <form id="frmnuevo">
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Tipo</label>
                                                <select class="form-control" id="des_tipo" name="des_tipo">
                                                    <option value="">Seleccionar</option>
                                                </select>
                                            </div>
                                            <div class="col">
                                                <label for="">Marca</label>
                                                <select class="form-control" id="des_marca" name="des_marca">
                                                    <option value="">Seleccionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Modelo</label>
                                                <select class="form-control" id="des_modelo" name="des_modelo">
                                                    <option value="">Seleccionar</option>
                                                </select>
                                            </div>
                                            <div class="col">
                                                <label for="">Serial</label>
                                                <input type="text" id="serial_imp" name="serial_imp" class="form-control" placeholder="Serial" pattern="[A-Za-z0-9]{1,20}">
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Serial Bien Nacional</label>
                                                <input type="text" id="serialb_imp" name="serialb_imp" class="form-control" placeholder="Serial Bien Nacional" pattern="[A-Za-z0-9]{1,20}">
                                            </div>
                                            <div class="col">
                                                <label for="">Status Impresora</label>
                                                <select class="form-control" id="status_imp" name="status_imp">
                                                    <option value="">Seleccionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Edificio.</label>
                                                <select class="form-control" id="des_edificio" name="des_edificio">
                                                    <option value="">Seleccionar</option>
                                                </select>
                                            </div>
                                            <div class="col">
                                                <label for="">Ubicacion.</label>
                                                <select class="form-control" id="des_ubicacion" name="des_ubicacion">
                                                    <option value="">Seleccionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Observación</label>
                                                <textarea class="form-control" id="observacion_imp" name="observacion_imp" rows="3" placeholder="Ingresar Observación..." pattern="[A-Za-z0-9]{1,500}"></textarea>
                                            </div>
                                        </div>
                                        <div class="float-right">
                                            <button type="button" id="guardarcambios" class="btn btn-primary">Guardar</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="agregarMarca" role="tabpanel" aria-labelledby="agregarMarca-tab">
                                <!--TAB AGREGAR MARCA-->
                                <div class="container">
                                    <h5 class="text-center">Agregar Detalles de Marca</h5>
                                    <form id="frmnuevamarca">
                                        <div class="form-row form-group">
                                            <div class="col-sm-6 m-auto">
                                                <label for="">Descripción </label>
                                                <input type="text" class="form-control" id="des_marcamarca" name="des_marcamarca" placeholder="Descripción" pattern="[A-Za-z0-9]{1,20}">
                                            </div>
                                        </div>
                                        <div class="float-right">
                                            <button type="button" id="guardarcambiosmarca" class="btn btn-primary">Guardar</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="agregarModelo" role="tabpanel" aria-labelledby="agregarModelo-tab">
                                <!--TAB AGREGAR MODELO-->
                                <div class="container">
                                    <h5 class="text-center">Agregar Detalles de Modelo</h5>
                                    <form id="frmnuevomodelo">
                                        <div class="form-row form-group">
                                            <div class="col-sm-6 m-auto">
                                                <label for="ubicacion">Tipo de Dispositivo</label>
                                                <select class="form-control" id="des_tipomodelo" name="des_tipomodelo">
                                                    <option value="">Seleccionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col-sm-6 m-auto">
                                                <label for="ubicacion">Marca</label>
                                                <select class="form-control" id="des_marcamodelo" name="des_marcamodelo">
                                                    <option value="">Seleccionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col-sm-6 m-auto">
                                                <label for="observacion">Descripción modelo.</label>
                                                <input type="text" class="form-control" id="des_modelomodelo" name="des_modelomodelo" placeholder="Descripción" pattern="[A-Za-z0-9]{1,20}">
                                            </div>
                                        </div>
                                        <div class="float-right">
                                            <button type="button" id="guardarcambiosmodelo" class="btn btn-primary">Guardar</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- TAB MODIFICAR IMPRESORA -->
    <div class="modal fade" id="modificar" tabindex="-1" role="dialog" aria-labelledby="agregarImpresora" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-body">
                        <div class="container">
                            <h5 class="text-center">Agregar Detalles de Impresora</h5>
                            <form id="frmnuevoU">
                                <div class="form-row form-group">
                                    <input type="text" hidden="" id="cod_impActu" name="cod_impActu">
                                    <div class="col">
                                        <label for="">Tipo</label>
                                        <select class="form-control" id="des_tipoActu" name="des_tipoActu">
                                            <option value="">Seleccionar</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label for="">Marca</label>
                                        <select class="form-control" id="des_marcaActu" name="des_marcaActu">
                                            <option value="">Seleccionar</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row form-group">
                                    <div class="col">
                                        <label for="">Modelo</label>
                                        <select class="form-control" id="des_modeloActu" name="des_modeloActu">
                                            <option value="">Seleccionar</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label for="">Serial</label>
                                        <input type="text" id="serial_impActu" name="serial_impActu" class="form-control" placeholder="Serial" pattern="[A-Za-z0-9]{10,20}">
                                    </div>
                                </div>
                                <div class="form-row form-group">
                                    <div class="col">
                                        <label for="">Serial Bien Nacional</label>
                                        <input type="text" id="serialb_impActu" name="serialb_impActu" class="form-control" placeholder="Serial Bien Nacional" pattern="[A-Za-z0-9]{10,20}">
                                    </div>
                                    <div class="col">
                                        <label for="">Status Impresora</label>
                                        <select class="form-control" id="status_impActu" name="status_impActu">
                                            <option value="">Seleccionar</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row form-group">
                                    <div class="col">
                                        <label for="">Edificio.</label>
                                        <select class="form-control" id="des_edificioActu" name="des_edificioActu">
                                            <option value="">Seleccionar</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label for="">Ubicación</label>
                                        <select class="form-control" id="des_ubicacionActu" name="des_ubicacionActu">
                                            <option value="">Seleccionar</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row form-group">
                                    <div class="col">
                                        <label for="">Observación</label>
                                        <textarea class="form-control" id="observacion_impActu" name="observacion_impActu" rows="3" placeholder="Ingresar Observación..."></textarea>
                                    </div>
                                </div>

                                <div class="float-right">
                                    <button type="button" class="btn btn-primary" id="actualizarimpresora">Guardar</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--MODAL COMPATIBILIDAD-->
    <div class="modal fade" id="agregarcompatibilidad" tabindex="-1" role="dialog" aria-labelledby="agregarImpresora" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!--TABS-->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
                </div>
                <div class="modal-body">
                    <!--TAB COMPATIBILIDAD IMPRESORA-->
                    <div class="container">
                        <h4 class="text-center mb-3">
                            Agregar compatibilidad
                        </h4>
                        <input type="text" hidden="" id="error" name="error" value="">
                        <form id="frmcompatibilidad">
                            <!-- Cartucho reporte -->
                            <div class="form-row form-group mb-4">
                                <div class="col">
                                    <label for="">Modelo impresora.</label>
                                    <input type="text" hidden="" id="modeloimpid" name="modeloimpid" value="">
                                    <input type="text" id="modeloimpd" name="modeloimpd" class="form-control" placeholder="Dirijase a tab cargar modelo impresora"
                                        disabled>
                                </div>
                                <div class="col">
                                    <label for="">Modelo toner.</label>
                                    <input type="text" hidden="" id="modelotid" name="modelotid" value="">
                                    <input type="text" id="modelotd" name="modelotd" class="form-control" placeholder="Dirijase a tab cargar modelo toner" disabled>
                                </div>
                            </div>
                            <ul class="nav nav-pills nav-fill mb-4" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#CargarModeloimpresora" role="tab" aria-controls="pills-profile"
                                        aria-selected="false">Cargar modelo impresora</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#cargarModelotoner" role="tab" aria-controls="pills-profile"
                                        aria-selected="false">Cargar modelo Toner</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="CargarModeloimpresora" role="tabpanel" aria-labelledby="agregarMarca-tab">
                                    <!--TAB AGREGAR MARCA-->
                                    <div class="container mb-4">
                                        <h5 class="text-center">Cargar modelo impresora</h5>
                                        <div class="container-fluid">
                                            <div class="table-responsive">
                                                <div id="tabladatamodeloimp"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="cargarModelotoner" role="tabpanel" aria-labelledby="agregarMarca-tab">
                                    <!--TAB CARGAR MODELO TONER-->
                                    <div class="container mb-4">
                                        <h5 class="text-center">Cargar modelo toner</h5>
                                        <div class="container-fluid">
                                            <div class="table-responsive">
                                                <div id="tabladatamodelotoner"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="float-right">
                                <button type="button" id="guardarcompatibilidad" class="btn btn-primary">Guardar</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

    <!--TABLA-->
    <div class="container-fluid">
        <div class="table-responsive">
            <div id="tabladatatable"></div>
        </div>
    </div>

    <!-- <div class="container-fluid">
        <div class="table-responsive">
            <div id="tablamantenimientos"></div>
        </div>
    </div> -->

</body>
<script src="librerias/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="librerias/alertify/alertify.js"></script>
<script src="librerias/fontawesome/svg-with-js/js/fontawesome-all.js"></script>
<script src="js/index.js"></script>
<script src="js/selecttoner.js"></script>

</html>