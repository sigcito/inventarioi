<?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();

    $cod_toner = $conexion->real_escape_string ($_POST['cod_toner']);

    $sql = "SELECT tcheck.id_revision, 
    tcheck.porcentaje, 
    tcheck.contador, 
    acc.descripcion, 
    tcheck.observaciones FROM toner_check tcheck LEFT JOIN stock_toner st ON st.id_stock_toner = tcheck.id_toner_stock LEFT JOIN acciones acc ON acc.id_accion=tcheck.id_accion WHERE id_toner_stock=$cod_toner";

    $result = mysqli_query($conexion,$sql);
?>

        <div>
            <table id="revisiones" class="table table-hover custom-table">
                <thead class="thead-dark">
                    <tr class="">
                        <th scope="col">#</th>
                        <th scope="col">ID Impresora</th>
                        <th scope="col">Fecha revision</th>
                        <th scope="col">Operador</th>
                        <th scope="col">Contador</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while ($mostrar=mysqli_fetch_row($result)){

                          /*  $datos=$mostrar[0]."||".
                                    $mostrar[1]."||".
                                    $mostrar[2]."||".
                                    $mostrar[3]."||".
                                    $mostrar[4]."||".
                                    $mostrar[5]."||".
                                    $mostrar[6]."||".
                                    $mostrar[7]."||".
                                    $mostrar[8]."||".
                                    $mostrar[9]."||".
                                    $mostrar[10]."||".
                                    $mostrar[11]."||".
                                    $mostrar[12]."||".
                                    $mostrar[13]."||".
                                    $mostrar[14]."||".
                                    $mostrar[15];*/
                    ?>
                <tr>
                    <td><?php echo $mostrar[0] ?></td>
                    <td><?php echo $mostrar[1] ?></td>
                    <td><?php echo $mostrar[2] ?></td>
                    <td><?php echo $mostrar[3] ?></td>
                    <td><?php echo $mostrar[4] ?></td>
                </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#revisiones').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                "infoEmpty": "Mostrando 0 to 0 of 0 Datoss",
                "infoFiltered": "(Filtrado de _MAX_ total datos)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Datos",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    });
</script>