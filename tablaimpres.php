<?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();

    $sql = "SELECT i.cod_imp,  /*0*/
    td.id_dispositivo,         /*1*/
    td.des_tipo,               /*2*/
    m.id_marca,                /*3*/
    m.des_marca,               /*4*/
    md.id_modelo,              /*5*/
    md.des_modelo,             /*6*/
    i.serial_imp,              /*7*/
    i.serialb_imp,             /*8*/
    s.cod_status,              /*9*/
    s.des_status,              /*10*/
    u.id_ubicacion,            /*11*/
    u.des_ubicacion,           /*12*/
    i.observacion_imp,         /*13*/
    e.id_edificio,             /*14*/ 
    e.descripcion             /*15*/ FROM impresora_fotocopiadora i LEFT JOIN status s ON s.cod_status = i.status_imp LEFT JOIN ubicacion u ON u.id_ubicacion = i.cod_ubicacion LEFT JOIN modelo_dispositivo md ON md.id_modelo = i.cod_modelo LEFT JOIN tipo_dispositivo td ON td.id_dispositivo = i.cod_tipo LEFT JOIN marca_dispositivo m ON m.id_marca = md.id_marca LEFT JOIN edificio e ON e.id_edificio = u.id_edf WHERE i.cod_imp";

    $result = mysqli_query($conexion,$sql);
?>

        <div>
            <table id="pagina" class="table table-hover custom-table">
                <thead class="thead-dark">
                    <tr class="">
                        <th class="align-middle" scope="col">#</th>
                        <th class="align-middle" scope="col">Serial</th>
                        <th class="align-middle" scope="col">Serial Bien ANTV</th>
                        <th class="align-middle" scope="col">Status</th>
                        <th class="align-middle" scope="col">Caracteristicas</th>
                        <th class="align-middle" scope="col">Ubicación</th>
                        <th class="align-middle" scope="col">Marca</th>
                        <th class="align-middle" scope="col">Modelo</th>
                        <th class="align-middle" scope="col">Tipo</th>
                        <th class="align-middle" scope="col"></th>
                        <th class="align-middle" scope="col"></th>
                        <th class="align-middle" scope="col"></th>
                        <th class="align-middle" scope="col"></th>
                        <th class="align-middle" scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while ($mostrar=mysqli_fetch_row($result)){

                            $datos=$mostrar[0]."||".
                                    $mostrar[1]."||".
                                    $mostrar[2]."||".
                                    $mostrar[3]."||".
                                    $mostrar[4]."||".
                                    $mostrar[5]."||".
                                    $mostrar[6]."||".
                                    $mostrar[7]."||".
                                    $mostrar[8]."||".
                                    $mostrar[9]."||".
                                    $mostrar[10]."||".
                                    $mostrar[11]."||".
                                    $mostrar[12]."||".
                                    $mostrar[13]."||".
                                    $mostrar[14]."||".
                                    $mostrar[15];
                    ?>
                <tr>
                    <td class="align-middle"><?php echo $mostrar[0]; ?></td>
                    <td class="align-middle"><?php echo $mostrar[7]; ?></td>
                    <td class="align-middle"><?php echo $mostrar[8]; ?></td>
                    <td class="align-middle"><?php echo $mostrar[10]; ?></td>
                    <td class="align-middle"><?php echo $mostrar[13]; ?></td>
                    <td class="align-middle"><?php echo $mostrar[12]; ?></td>
                    <td class="align-middle"><?php echo $mostrar[4]; ?></td>
                    <td class="align-middle"><?php echo $mostrar[6]; ?></td>
                    <td class="align-middle"><?php echo $mostrar[2]; ?></td>
                    <td style="text-align: center;">
                        <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modificar" onclick="agregaform('<?php echo $datos;?>')"> <i class="fas fa-pencil-alt fa-lg"></i></span>
                    </td>
                    <td style="text-align: center;">
                        <span class="btn btn-danger btn-sm" data-toggle="modal" data-target="#eliminar" onclick="segurodeeliminardatos('<?php echo $datos;?>')"><i class="fas fa-trash-alt fa-lg"></i></span>
                    </td>
                    <td>
                        <a class="btn btn-dark" href="cartuchos_instalados.php?datos=<?php echo $mostrar[0];?>">Toners</a>
                    </td>
                    <td style="text-align: center;">
                        <a class="btn btn-success" href="imp_revisiones.php?datos=<?php echo $mostrar[0];?>"><i class="far fa-plus-square fa-lg"></i> Revision</a>
                    </td>
                    <td style="text-align: center;">
                        <a class="btn btn-info" href="asociar_toner.php?datos=<?php echo $mostrar[0];?>&modelo=<?php echo $mostrar[5];?>"><i class="far fa-plus-square fa-lg"></i> Asociar Toner</a>
                    </td>
                </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function () {
	$('#pagina').DataTable({
		"language": {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
			"infoEmpty": "Mostrando 0 to 0 of 0 Datos",
			"infoFiltered": "(Filtrado de _MAX_ total datos)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Datos",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		}
    });
});
</script>
