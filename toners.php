<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
    <title>APP IMPRESORAS</title>
</head>

<body>
    <!--CABECERA-->
    <div class="container-fluid">
        <h1 class="text-center mb-5 mt-4">Listado de toners</h1>
    </div>
    <div class="container-fluid">
        <nav class="navbar justify-content-center">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#agregarToner">Agregar toner</button>
            <a class="btn btn-dark ml-4" href="index.php">Volver a index.</a>
        </nav>
        <div class="modal fade" id="agregarToner" tabindex="-1" role="dialog" aria-labelledby="agregarToner" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!--TABS TONER-->
                    <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="tonerStock-tab" data-toggle="pill" href="#tonerStock" role="tab" aria-controls="tonerStock"
                                aria-selected="true">Agregar Toner Stock</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tonerModelo-tab" data-toggle="pill" href="#tonerModelo" role="tab" aria-controls="tonerModelo" aria-selected="false">Agregar Modelo Toner</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tonerMarca-tab" data-toggle="pill" href="#tonerMarca" role="tab" aria-controls="tonerMarca" aria-selected="false">Agregar Marca Toner</a>
                        </li>
                    </ul>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tab-content" id="pills-tabContent">
                        <!--TAB AGREGAR TONER STOCK-->
                        <div class="tab-pane fade show active" id="tonerStock" role="tabpanel" aria-labelledby="tonerStock-tab">
                            <div class="container">
                                <h5 class="text-center">Agregar Toner Stock</h5>
                                <form id="frmaddtoner_stock">
                                    <input type="text" hidden="" class="form-control" placeholder="Descripción" id="id_default" name="id_default" value="0">
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <label for="">Serial Caja</label>
                                            <input type="text" class="form-control" name="serial_caja" id="serial_caja" placeholder="Ingrese serial de caja.">
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <label for="">Fecha de ingreso</label>
                                            <input type="date" class="form-control" name="f_ingresoStock" id="f_ingresoStock" placeholder="Fecha de ingreso">
                                        </div>
                                        <div class="col">
                                            <label for="">Contador</label>
                                            <input type="text" class="form-control" placeholder="Contador" name="contadorStock" id="contadorStock">
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <label for="">Marca</label>
                                            <select class="form-control" id="cod_marcaStock" name="cod_marcaStock">
                                                <option value="">Seleccionar</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label for="">Modelo</label>
                                            <select class="form-control" name="cod_modeloStock" id="cod_modeloStock">
                                                <option value="">Seleccionar</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <label for="">Status condicion</label>
                                            <select class="form-control" name="status_condicionStock" id="status_condicionStock">
                                                <option value="">Seleccionar</option>
                                            </select>
                                        </div>
                                        <!-- <div class="col-sm-6">
                                            <label for="">Status Stock/Uso.</label> -->
                                        <input type="text" hidden="" class="form-control" placeholder="Contador" name="statusdefault" id="statusdefault" value="0">
                                        <!-- </div> -->
                                    </div>
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <label for="">Observación</label>
                                            <textarea class="form-control" id="observacionStock" name="observacionStock" rows="3" placeholder="Ingresar Observación..."></textarea>
                                        </div>
                                    </div>
                                    <div class="float-right">
                                        <button id="guardartonerstock" type="button" class="btn btn-primary">Guardar</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--TAB AGREGAR MODELO TONER-->
                        <div class="tab-pane fade" id="tonerModelo" role="tabpanel" aria-labelledby="tonerModelo-tab">
                            <div class="container">
                                <h5 class="text-center">Agregar Modelo Toner</h5>
                                <form id="frmaddmodelo_toner">
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <label for="">Serial modelo</label>
                                            <input type="text" class="form-control" placeholder="Descripción" id="des_modeloToner" name="des_modeloToner">
                                        </div>
                                        <div class="col">
                                            <label for="">Marca</label>
                                            <select class="form-control" id="id_marcaToner" name="id_marcaToner">
                                                <option value="">Seleccionar</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <label for="">Color</label>
                                            <input type="text" class="form-control" placeholder="Color" id="colorToner" name="colorToner" list="color">
                                            <datalist id="color">
                                                <option value="BLACK">
                                                <option value="CYAN">
                                                <option value="YELLOW">
                                                <option value="MAGENTA">
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="float-right">
                                        <button type="button" class="btn btn-primary" id="guardarmodelo_toner" name="guardarmodelo_toner">Guardar</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--TAB AGREGAR MARCA TONER-->
                        <div class="tab-pane fade" id="tonerMarca" role="tabpanel" aria-labelledby="tonerMarca-tab">
                            <div class="container">
                                <h5 class="text-center">Agregar Marca Toner</h5>
                                <form id="frmaddmarca_toner">
                                    <div class="form-row form-group">
                                        <div class="col-sm-6 m-auto">
                                            <label for="">Descripción</label>
                                            <input type="text" class="form-control" placeholder="Descripción" id="des_marcaToner" name="des_marcaToner">
                                        </div>
                                    </div>
                                    <div class="float-right">
                                        <button type="button" class="btn btn-primary" id="guardarmarca_toner" name="guardarmarca_toner">Guardar</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="actualizarToner" tabindex="-1" role="dialog" aria-labelledby="agregarToner" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                        <!--TAB AGREGAR TONER STOCK-->
                        <div class="tab-pane fade show active" id="tonerStock" role="tabpanel" aria-labelledby="tonerStock-tab">
                            <div class="container">
                                <h5 class="text-center">Agregar Toner Stock</h5>
                                <form id="frmaddtoner_stock">
                                    <input type="text" hidden="" class="form-control" placeholder="Descripción" id="id_default" name="id_default" value="0">
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <label for="">Serial Cartucho</label>
                                            <input type="text" class="form-control" name="serial_cartuchoStockActu" id="serial_cartuchoStockActu" placeholder="Ingrese serial cartucho.">
                                        </div>
                                        <div class="col">
                                            <label for="">Fecha de ingreso</label>
                                            <input type="text" class="form-control" name="f_ingresoStockActu" id="f_ingresoStockActu" placeholder="Fecha de ingreso">
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <label for="">Marca</label>
                                            <select class="form-control" id="cod_marcaStockActu" name="cod_marcaStockActu">
                                                <option value="">Seleccionar</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label for="">Modelo</label>
                                            <select class="form-control" name="cod_modeloStockActu" id="cod_modeloStockActu">
                                                <option value="">Seleccionar</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <label for="">Status condicion</label>
                                            <select class="form-control" name="status_condicionStockActu" id="status_condicionStockActu">
                                                <option value="">Seleccionar</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <label for="">Observación</label>
                                            <textarea class="form-control" id="observacionStockActu" name="observacionStockActu" rows="3" placeholder="Ingresar Observación..."></textarea>
                                        </div>
                                    </div>
                                    <div class="float-right">
                                        <button id="actualizartoner" type="button" class="btn btn-primary">Guardar</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--TABLA-->

    <div class="container-fluid">
        <div class="table-responsive">
            <div id="tablatoners"></div>
        </div>
    </div>

</body>
<script src="librerias/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="librerias/alertify/alertify.js"></script>
<script src="librerias/fontawesome/svg-with-js/js/fontawesome-all.js"></script>
<script src="js/index.js"></script>
<script src="js/selecttoner.js"></script>

</html>