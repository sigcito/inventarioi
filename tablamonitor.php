<?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();

    $sql = "SELECT m.id_monitor,    /*0*/
    m.descripcion,                /*1*/ 
    m.id_marca                    /*2*/ 
    FROM monitor m WHERE m.id_monitor";

    $result = mysqli_query($conexion,$sql);
?>

        <div>
            <table id="tablamonitor" class="table table-hover custom-table" data-page-length='5'>
                <thead class="thead-dark">
                    <tr class="">
                        <th scope="col">#</th>
                        <th scope="col">Serial</th>
                        <th scope="col">Marca</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while ($mostrar=mysqli_fetch_row($result)){

                            $datosmoni=$mostrar[0]."||".
                                    $mostrar[1]."||".
                                    $mostrar[2];
                    ?>
                <tr>
                    <td><?php echo $mostrar[0] ?></td>
                    <td><?php echo $mostrar[1] ?></td>
                    <td><?php echo $mostrar[2] ?></td>
                    <td style="text-align: center;">
                        <span class="btn btn-primary btn-sm" onclick="agregamoni('<?php echo $datosmoni;?>')"> <i class="fas fa-pencil-alt fa-lg"></i></span>
                    </td>
                </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        
<script type="text/javascript">
    $(document).ready(function () {
	$('#tablamonitor').DataTable({
		"language": {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
			"infoEmpty": "Mostrando 0 to 0 of 0 Datoss",
			"infoFiltered": "(Filtrado de _MAX_ total datos)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Datos",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		}
    });
});
</script>
