-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 30-07-2018 a las 15:52:00
-- Versión del servidor: 10.1.26-MariaDB-0+deb9u1
-- Versión de PHP: 7.0.30-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `impresoras`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acciones`
--

CREATE TABLE `acciones` (
  `id_accion` tinyint(1) NOT NULL,
  `descripcion` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `acciones`
--

INSERT INTO `acciones` (`id_accion`, `descripcion`) VALUES
(1, 'Accion Prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compatibilidad`
--

CREATE TABLE `compatibilidad` (
  `id_compatibilidad` int(3) NOT NULL,
  `id_modelo_impresora` int(3) NOT NULL,
  `id_modelo_toner` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compatibilidad`
--

INSERT INTO `compatibilidad` (`id_compatibilidad`, `id_modelo_impresora`, `id_modelo_toner`) VALUES
(5, 21, 7),
(6, 3, 1),
(7, 1, 12),
(8, 14, 12),
(9, 14, 12),
(10, 16, 13),
(11, 14, 15),
(12, 1, 12),
(13, 1, 45),
(14, 12, 45),
(15, 12, 12),
(16, 14, 15),
(17, 12, 14),
(18, 9, 13),
(19, 17, 36),
(20, 17, 37),
(21, 17, 38),
(22, 17, 39),
(23, 4, 35),
(24, 5, 35),
(25, 10, 35),
(26, 11, 35),
(27, 3, 31),
(28, 3, 32),
(29, 3, 33),
(30, 3, 33),
(31, 3, 34),
(32, 16, 29),
(33, 16, 28),
(34, 22, 27);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edificio`
--

CREATE TABLE `edificio` (
  `id_edificio` tinyint(1) NOT NULL,
  `descripcion` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `edificio`
--

INSERT INTO `edificio` (`id_edificio`, `descripcion`) VALUES
(1, 'C.C. Capitolio - Piso 9'),
(2, 'Edificio 5'),
(3, 'C.C Capitolio - Piso 10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id_empresa` tinyint(3) NOT NULL,
  `nombre` varchar(70) NOT NULL,
  `rif` varchar(10) NOT NULL,
  `tlf_contacto` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impresora_fotocopiadora`
--

CREATE TABLE `impresora_fotocopiadora` (
  `cod_imp` int(3) NOT NULL,
  `serial_imp` varchar(20) NOT NULL,
  `serialb_imp` varchar(20) NOT NULL,
  `cod_modelo` int(3) NOT NULL,
  `status_imp` int(3) NOT NULL,
  `cod_tipo` int(3) NOT NULL,
  `observacion_imp` varchar(100) NOT NULL,
  `cod_ubicacion` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `impresora_fotocopiadora`
--

INSERT INTO `impresora_fotocopiadora` (`cod_imp`, `serial_imp`, `serialb_imp`, `cod_modelo`, `status_imp`, `cod_tipo`, `observacion_imp`, `cod_ubicacion`) VALUES
(59, 'VND3D24004', '2147483647', 14, 3, 1, 'Funcional / Falta Mantenimiento / Posee Toner', 86),
(60, '7031701391', '2147483647', 16, 3, 1, 'OFICINA P-9 SOPORTE, Falta Cartucho Negro\r\n', 86),
(61, 'CNB2021925', '2147483647', 17, 1, 1, 'FUNCIONANDO / Fallas en la difusora, mancha los documentos (Mantenimiento Urgente para la Gerente)\r\n', 81),
(62, 'E8BY117196', '0', 22, 4, 1, 'Para desincorporación', 86),
(63, 'DXA898532', '2147483647', 23, 1, 3, 'Funcional / Falta Mantenimiento y Toners\r\n', 86),
(64, 'CNBF314811', '2147483647', 17, 3, 1, 'No imprime por falta de mantenimiento/No hay toners (NEGRO Y AMARILLO) de repuesto en el inventario\r', 86),
(65, 'CNB1D07513', '2147483647', 24, 3, 1, 'Estatus Desconocido / Falta Cartucho', 86),
(66, 'CNF87DM00V', '2147483647', 25, 3, 3, 'Estatus Desconocido / Falta Cartucho\r\n\r\n\r\n', 88),
(67, 'CNF87DL0GW', '0', 25, 3, 3, 'Estatus Desconocido / Falta Cartucho\r\n\r\n\r\n', 88),
(68, 'SDG0B-0604-03', '0', 26, 3, 1, 'IMPRESORA, TELÉFONO, FAX; Estatus Desconocido / Falta Cartucho', 88),
(69, '85044154', 'S/N', 27, 3, 2, 'Por servicio técnico general', 84);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mantenimientos`
--

CREATE TABLE `mantenimientos` (
  `id_mantenimientos` int(5) NOT NULL,
  `id_impresora` int(3) NOT NULL,
  `f_mantenimiento` date NOT NULL,
  `id_operador` smallint(3) NOT NULL,
  `id_tipo_mant` tinyint(1) NOT NULL,
  `id_empresa` tinyint(3) NOT NULL,
  `observaciones` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca_dispositivo`
--

CREATE TABLE `marca_dispositivo` (
  `id_marca` int(3) NOT NULL,
  `des_marca` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca_dispositivo`
--

INSERT INTO `marca_dispositivo` (`id_marca`, `des_marca`) VALUES
(17, ' HP'),
(23, 'Canon'),
(24, 'Epson'),
(22, 'Sharp'),
(25, 'Wilsonl'),
(18, 'Xerox');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca_toner`
--

CREATE TABLE `marca_toner` (
  `id_marca_toner` int(11) NOT NULL,
  `descripcion` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca_toner`
--

INSERT INTO `marca_toner` (`id_marca_toner`, `descripcion`) VALUES
(1, 'HP'),
(2, 'Laser Toner Cartridge'),
(3, 'Cannon'),
(4, 'Xerox'),
(5, 'wilsonlacra'),
(6, 'SHARP'),
(7, 'EPSON');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelo_dispositivo`
--

CREATE TABLE `modelo_dispositivo` (
  `id_modelo` int(3) NOT NULL,
  `des_modelo` varchar(50) NOT NULL,
  `id_dispositivo` int(3) NOT NULL DEFAULT '1',
  `id_marca` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modelo_dispositivo`
--

INSERT INTO `modelo_dispositivo` (`id_modelo`, `des_modelo`, `id_dispositivo`, `id_marca`) VALUES
(1, 'Laserjet 4515n', 1, 17),
(2, 'AR-M257', 1, 22),
(3, 'Laserjet 500 Color M551', 1, 17),
(4, 'Laserjet 4250', 1, 17),
(5, 'Laserjet 4250n', 1, 17),
(9, 'Laserjet P1505', 1, 17),
(10, 'Laserjet P4015n', 1, 17),
(11, 'Laserjet 4350n', 1, 17),
(12, 'Laserjet 600  M602', 1, 17),
(14, 'Laserjet P1102w', 1, 17),
(16, 'Color Laserjet 2600n', 1, 17),
(17, 'CP1025nw Color Laserjet', 1, 17),
(18, 'Officejet 100 mobile Printer', 1, 17),
(20, 'Laserjet 1536 dnf Mfp', 1, 17),
(21, 'winson 1 Lacra', 1, 25),
(22, 'FX-890', 1, 24),
(23, 'PHASER 6110 MFP', 3, 18),
(24, 'LASERJET1160', 1, 17),
(25, 'Color Laserjet CM1015 MFP', 3, 17),
(26, 'OFFICEJET J3600', 1, 17),
(27, 'AR310NT', 2, 22),
(29, 'LaserJet Color 3600n', 1, 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelo_toner`
--

CREATE TABLE `modelo_toner` (
  `id_modelo_toner` int(3) NOT NULL,
  `serial_modelo` varchar(25) NOT NULL,
  `id_marca_toner` int(3) NOT NULL,
  `color` varchar(20) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modelo_toner`
--

INSERT INTO `modelo_toner` (`id_modelo_toner`, `serial_modelo`, `id_marca_toner`, `color`) VALUES
(1, 'CX400A', 1, 'BLACK'),
(4, 'CX401A', 1, 'CYAN'),
(6, 'CX402B', 1, 'MAGENTA'),
(9, 'AR-M257', 6, 'BLACK'),
(10, 'AR-310NT', 6, 'BLACK'),
(12, 'CC364A', 1, 'BLACK'),
(13, 'CB436A', 1, 'Black'),
(14, 'CC390A', 1, 'Black'),
(15, 'CE285A', 1, 'BLACK'),
(20, 'Q6472A', 1, 'YELLOW'),
(21, 'Q6471A', 1, 'CYAN'),
(22, 'Q6470A', 1, 'BLACK'),
(23, 'Q6473A', 1, 'MAGENTA'),
(24, 'LHCE321A', 1, 'CYAN'),
(26, 'LHCE323A', 1, 'MAGENTA'),
(27, 'S015329', 7, 'BLACK'),
(28, 'LH6003A', 1, 'MAGENTA'),
(29, 'Q6003A', 1, 'MAGENTA'),
(30, 'CB435A', 1, 'BLACK'),
(31, 'CE400X', 1, 'BLACK'),
(32, 'CE401A', 1, 'CYAN'),
(33, 'CE402A', 1, 'YELLOW'),
(34, 'CE403A', 1, 'MAGENTA'),
(35, 'Q5942A', 1, 'BLACK'),
(36, 'CE310A', 1, 'BLACK'),
(37, 'CE311A', 1, 'CYAN'),
(38, 'CE312A', 1, 'YELLOW'),
(39, 'CE313A', 1, 'MAGENTA'),
(45, 'CE390A', 1, 'BLACK');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operadores`
--

CREATE TABLE `operadores` (
  `id_operador` smallint(3) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `apellido` varchar(15) NOT NULL,
  `cedula` int(9) NOT NULL,
  `secret` varchar(60) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `id_rol` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `operadores`
--

INSERT INTO `operadores` (`id_operador`, `nombre`, `apellido`, `cedula`, `secret`, `usuario`, `id_rol`) VALUES
(1, 'Alejandro', 'Gonzalez', 25626512, '123456', 'admin', 1),
(2, 'Luis', 'Mora', 27467562, '27467562l', '27467562', 1),
(5, 'Wilnor', 'Lugo', 25258151, '25258151W', 'wilnor', 1),
(6, 'Edgar', 'Garcia', 15894123, '15894123', 'Danieledg', 1),
(7, 'Rafael', 'Vivas', 16134997, '16134997R', 'rojovivas', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `revisiones`
--

CREATE TABLE `revisiones` (
  `id_revision` int(4) NOT NULL,
  `id_impresora` int(3) NOT NULL,
  `f_ejecucion` varchar(15) CHARACTER SET utf8 NOT NULL,
  `id_operador` smallint(3) NOT NULL,
  `contador_impresora` int(5) NOT NULL,
  `observaciones` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_rol` tinyint(1) NOT NULL,
  `descripcion` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `descripcion`) VALUES
(1, 'Administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE `status` (
  `cod_status` int(3) NOT NULL,
  `des_status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`cod_status`, `des_status`) VALUES
(1, 'Operativa'),
(2, 'Operativa-En resguardo'),
(3, 'Inactiva-Mantenimiento'),
(4, 'Inactiva-Dañada'),
(5, 'Inactiva-Retirada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `statusstocktoner`
--

CREATE TABLE `statusstocktoner` (
  `id_sta_stock` int(1) NOT NULL,
  `descripcion` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `statusstocktoner`
--

INSERT INTO `statusstocktoner` (`id_sta_stock`, `descripcion`) VALUES
(1, 'Activo'),
(2, 'Stock');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_toner`
--

CREATE TABLE `status_toner` (
  `cod_statustoner` int(2) NOT NULL,
  `des_status_toner` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `status_toner`
--

INSERT INTO `status_toner` (`cod_statustoner`, `des_status_toner`) VALUES
(1, 'ok-nuevo'),
(2, 'ok-nuevo poco uso'),
(3, 'Se bota el toner o tinta'),
(4, 'Dañado'),
(5, 'Vacio'),
(6, 'Dañado de Fabrica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock_toner`
--

CREATE TABLE `stock_toner` (
  `id_stock_toner` int(4) NOT NULL,
  `serial_cartucho` varchar(25) NOT NULL,
  `cod_modelo` int(4) NOT NULL,
  `f_ingreso` varchar(15) NOT NULL,
  `observacion` text NOT NULL,
  `id_status_condicion` tinyint(1) NOT NULL,
  `id_status_stock` tinyint(1) NOT NULL,
  `cod_imp` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `stock_toner`
--

INSERT INTO `stock_toner` (`id_stock_toner`, `serial_cartucho`, `cod_modelo`, `f_ingreso`, `observacion`, `id_status_condicion`, `id_status_stock`, `cod_imp`) VALUES
(6, 'CB436A', 13, '07/18/2018', 'Toner Generico', 1, 1, 60),
(7, 'S/N', 15, '2018-07-25', 'GENERICO', 5, 0, 0),
(8, 'S/N', 31, '2018-07-25', 'EN EL ALMACEN', 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tecnicos`
--

CREATE TABLE `tecnicos` (
  `id_tecnico` int(4) NOT NULL,
  `cedula` int(9) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `apellido` varchar(15) NOT NULL,
  `id_empresa` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tecnicos`
--

INSERT INTO `tecnicos` (`id_tecnico`, `cedula`, `nombre`, `apellido`, `id_empresa`) VALUES
(1, 25626512, 'Alejandro', 'Gonzalez', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tecnicos_mantenimiento`
--

CREATE TABLE `tecnicos_mantenimiento` (
  `id_tec_mant` int(4) NOT NULL,
  `id_tecnico` int(4) NOT NULL,
  `id_mantenimiento` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_dispositivo`
--

CREATE TABLE `tipo_dispositivo` (
  `id_dispositivo` int(3) NOT NULL,
  `des_tipo` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_dispositivo`
--

INSERT INTO `tipo_dispositivo` (`id_dispositivo`, `des_tipo`) VALUES
(1, 'Impresora'),
(2, 'Impresora-Fotocopiadora'),
(3, 'Impresora Multifuncional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_mantenimiento`
--

CREATE TABLE `tipo_mantenimiento` (
  `id_tipo_mant` tinyint(1) NOT NULL,
  `descripcion` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_mantenimiento`
--

INSERT INTO `tipo_mantenimiento` (`id_tipo_mant`, `descripcion`) VALUES
(1, 'Correctivo'),
(2, 'Preventivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `toner_check`
--

CREATE TABLE `toner_check` (
  `id_toner_check` int(4) NOT NULL,
  `id_revision` int(4) NOT NULL,
  `id_toner_stock` int(4) NOT NULL,
  `porcentaje` tinyint(2) NOT NULL,
  `contador` int(5) NOT NULL,
  `id_accion` tinyint(1) NOT NULL,
  `observaciones` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubicacion`
--

CREATE TABLE `ubicacion` (
  `id_ubicacion` int(2) NOT NULL,
  `des_ubicacion` varchar(80) NOT NULL,
  `id_edf` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ubicacion`
--

INSERT INTO `ubicacion` (`id_ubicacion`, `des_ubicacion`, `id_edf`) VALUES
(55, 'Piso 2/ Produccion', 2),
(2, 'Piso 6/ Telepuerto', 2),
(3, 'Piso 4/ Servicio Generales Oficina Coordinador', 2),
(4, 'Piso 4/ Servicio Generales Oficina exteriores', 2),
(5, 'Piso 4/ Mini ANTV', 2),
(6, 'Piso 4/ Master', 2),
(7, 'Piso 4/ Control de Estudio', 2),
(8, 'Piso 4/ Programacion', 2),
(9, 'Piso 4/ Posproduccion', 2),
(10, 'Piso 4/ Imagen y Promociones Arte', 2),
(11, 'Piso 3 / Gerencia de operaciones', 2),
(12, 'Piso 4/ centro de copiado (ingesta)', 2),
(13, 'Piso 3/ Estudio Noticiero', 2),
(14, 'Piso 3/ Opinion, Informacion, Pagina Web', 2),
(15, 'Piso 3/ Archivo', 2),
(16, 'Piso 3/ voz off', 2),
(17, 'Piso 3/ Tecnologia', 2),
(18, 'Piso 2 / secretaria Gerencia General', 2),
(19, 'Piso 2 / Gerencia General', 2),
(20, 'Piso 2/ imagen y promociones / pecera', 2),
(21, 'Piso 2/ Comedor', 2),
(22, 'Piso 1/ Servicios a la produccion', 2),
(23, 'Piso 1/ Almacen de Ing.', 2),
(24, 'Piso 1/ Ingenieria/ Transmisiones', 2),
(25, 'Piso 1/ Equipamiento', 2),
(26, 'Piso 1/ Gerencia General\r\n', 2),
(27, 'PB/Seguridad\r\n', 2),
(28, 'PB/Protocolo\r\n', 2),
(29, 'PB/ Relaciones Institucionales\r\n', 2),
(30, 'PB/ Recepcion\r\n', 2),
(92, 'Piso 10/ Política', 3),
(93, 'Piso 10/ Archivo', 3),
(94, 'Piso 10/ Estudio 3', 3),
(80, 'C.C. Capitolio piso 9 / Comedor', 1),
(81, 'C.C. Capitolio piso 9 / Consultoría Jurídica', 1),
(82, 'C.C. Capitolio piso 9 / Compras', 1),
(83, 'C.C. Capitolio piso 9 / Planificación y presupuesto', 1),
(84, 'C.C. Capitolio piso 9 / Recursos Humanos', 1),
(85, 'C.C. Capitolio piso 9 / Servicios Generales', 1),
(86, 'C.C. Capitolio piso 9 / Soporte tecnológico (soporte)', 1),
(87, 'C.C. Capitolio piso 9 / Soporte tecnológico (sistemas) ', 1),
(88, 'C.C. Capitolio piso 9 / Almacén', 1),
(89, 'C.C. Capitolio piso 9 / Formación', 1),
(90, 'C.C. Capitolio piso 9 / Recepción', 1),
(91, 'C.C. Capitolio piso 9 / Oficina gerente', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acciones`
--
ALTER TABLE `acciones`
  ADD PRIMARY KEY (`id_accion`);

--
-- Indices de la tabla `compatibilidad`
--
ALTER TABLE `compatibilidad`
  ADD PRIMARY KEY (`id_compatibilidad`);

--
-- Indices de la tabla `edificio`
--
ALTER TABLE `edificio`
  ADD PRIMARY KEY (`id_edificio`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id_empresa`);

--
-- Indices de la tabla `impresora_fotocopiadora`
--
ALTER TABLE `impresora_fotocopiadora`
  ADD PRIMARY KEY (`cod_imp`),
  ADD UNIQUE KEY `serial_imp` (`serial_imp`,`cod_modelo`);

--
-- Indices de la tabla `mantenimientos`
--
ALTER TABLE `mantenimientos`
  ADD PRIMARY KEY (`id_mantenimientos`);

--
-- Indices de la tabla `marca_dispositivo`
--
ALTER TABLE `marca_dispositivo`
  ADD PRIMARY KEY (`id_marca`),
  ADD UNIQUE KEY `des_marca` (`des_marca`);

--
-- Indices de la tabla `marca_toner`
--
ALTER TABLE `marca_toner`
  ADD PRIMARY KEY (`id_marca_toner`);

--
-- Indices de la tabla `modelo_dispositivo`
--
ALTER TABLE `modelo_dispositivo`
  ADD PRIMARY KEY (`id_modelo`),
  ADD UNIQUE KEY `des_modelo` (`des_modelo`,`id_marca`);

--
-- Indices de la tabla `modelo_toner`
--
ALTER TABLE `modelo_toner`
  ADD PRIMARY KEY (`id_modelo_toner`),
  ADD UNIQUE KEY `serial_modelo` (`serial_modelo`);

--
-- Indices de la tabla `operadores`
--
ALTER TABLE `operadores`
  ADD PRIMARY KEY (`id_operador`),
  ADD UNIQUE KEY `usuario` (`usuario`),
  ADD UNIQUE KEY `cedula` (`cedula`);

--
-- Indices de la tabla `revisiones`
--
ALTER TABLE `revisiones`
  ADD PRIMARY KEY (`id_revision`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`cod_status`);

--
-- Indices de la tabla `statusstocktoner`
--
ALTER TABLE `statusstocktoner`
  ADD PRIMARY KEY (`id_sta_stock`);

--
-- Indices de la tabla `status_toner`
--
ALTER TABLE `status_toner`
  ADD PRIMARY KEY (`cod_statustoner`);

--
-- Indices de la tabla `stock_toner`
--
ALTER TABLE `stock_toner`
  ADD PRIMARY KEY (`id_stock_toner`),
  ADD UNIQUE KEY `id_stock_toner` (`id_stock_toner`),
  ADD KEY `cod_imp` (`cod_imp`),
  ADD KEY `cod_modelo` (`cod_modelo`);

--
-- Indices de la tabla `tecnicos`
--
ALTER TABLE `tecnicos`
  ADD PRIMARY KEY (`id_tecnico`);

--
-- Indices de la tabla `tecnicos_mantenimiento`
--
ALTER TABLE `tecnicos_mantenimiento`
  ADD PRIMARY KEY (`id_tec_mant`);

--
-- Indices de la tabla `tipo_dispositivo`
--
ALTER TABLE `tipo_dispositivo`
  ADD PRIMARY KEY (`id_dispositivo`);

--
-- Indices de la tabla `tipo_mantenimiento`
--
ALTER TABLE `tipo_mantenimiento`
  ADD PRIMARY KEY (`id_tipo_mant`);

--
-- Indices de la tabla `toner_check`
--
ALTER TABLE `toner_check`
  ADD PRIMARY KEY (`id_toner_check`);

--
-- Indices de la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  ADD PRIMARY KEY (`id_ubicacion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acciones`
--
ALTER TABLE `acciones`
  MODIFY `id_accion` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `compatibilidad`
--
ALTER TABLE `compatibilidad`
  MODIFY `id_compatibilidad` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `edificio`
--
ALTER TABLE `edificio`
  MODIFY `id_edificio` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id_empresa` tinyint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `impresora_fotocopiadora`
--
ALTER TABLE `impresora_fotocopiadora`
  MODIFY `cod_imp` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT de la tabla `mantenimientos`
--
ALTER TABLE `mantenimientos`
  MODIFY `id_mantenimientos` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `marca_dispositivo`
--
ALTER TABLE `marca_dispositivo`
  MODIFY `id_marca` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `marca_toner`
--
ALTER TABLE `marca_toner`
  MODIFY `id_marca_toner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `modelo_dispositivo`
--
ALTER TABLE `modelo_dispositivo`
  MODIFY `id_modelo` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `modelo_toner`
--
ALTER TABLE `modelo_toner`
  MODIFY `id_modelo_toner` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT de la tabla `operadores`
--
ALTER TABLE `operadores`
  MODIFY `id_operador` smallint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `revisiones`
--
ALTER TABLE `revisiones`
  MODIFY `id_revision` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_rol` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `cod_status` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `statusstocktoner`
--
ALTER TABLE `statusstocktoner`
  MODIFY `id_sta_stock` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `status_toner`
--
ALTER TABLE `status_toner`
  MODIFY `cod_statustoner` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `stock_toner`
--
ALTER TABLE `stock_toner`
  MODIFY `id_stock_toner` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `tecnicos`
--
ALTER TABLE `tecnicos`
  MODIFY `id_tecnico` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tecnicos_mantenimiento`
--
ALTER TABLE `tecnicos_mantenimiento`
  MODIFY `id_tec_mant` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipo_dispositivo`
--
ALTER TABLE `tipo_dispositivo`
  MODIFY `id_dispositivo` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tipo_mantenimiento`
--
ALTER TABLE `tipo_mantenimiento`
  MODIFY `id_tipo_mant` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `toner_check`
--
ALTER TABLE `toner_check`
  MODIFY `id_toner_check` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  MODIFY `id_ubicacion` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
