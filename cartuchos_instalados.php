<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
    <title>APP IMPRESORAS</title>
</head>

<body>
    <!--CABECERA-->
    <div class="container">
        <h1 class="text-center mb-5 mt-4">Cartuchos Instalados</h1>
    </div>
    <!--TABLA-->
    <div class="container-fluid">
        <div class="table-responsive">
            <?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();
    $impresoraid = $conexion->real_escape_string ($_GET ['datos']);

    $sql = "SELECT st.id_stock_toner,   /* 0 */
    mt.serial_modelo,                   /* 1 */
    st.serial_cartucho,                 /* 2 */
    mat.descripcion,                    /* 3 */
    mt.color,                           /* 4 */
    st.f_ingreso,                       /* 5 */
    st.observacion,                     /* 6 */
    id_status_condicion,                /* 7 */
    id_status_stock,                    /* 8 */
    st.cod_imp,                         /* 9 */
    st.cod_modelo,                      /* 10 */
    stat.des_status_toner               /* 11 */
    FROM stock_toner st LEFT JOIN modelo_toner mt ON mt.id_modelo_toner = st.cod_modelo LEFT JOIN marca_toner mat ON mat.id_marca_toner = mt.id_marca_toner LEFT JOIN status_toner stat ON stat.cod_statustoner = st.id_status_condicion WHERE st.cod_imp=$impresoraid AND id_status_stock=1";

    $result = mysqli_query($conexion,$sql);
?>

            <div>
                <table id="paginatoner" class="table table-hover custom-table">
                    <thead class="thead-dark">
                        <tr class="">
                            <th scope="col">#</th>
                            <th scope="col">Serial</th>
                            <th scope="col">Serial cartucho</th>
                            <th scope="col">Marca</th>
                            <th scope="col">Color</th>
                            <th scope="col">Fecha Ingreso</th>
                            <th scope="col">Observacion</th>
                            <th scope="col">Status</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($mostrar=mysqli_fetch_row($result)){
                            $datosIns=$mostrar[0]."||".
                            $mostrar[1]."||".
                            $mostrar[2]."||".
                            $mostrar[3]."||".
                            $mostrar[4]."||".
                            $mostrar[5]."||".
                            $mostrar[6]."||".
                            $mostrar[7]."||".
                            $mostrar[8]."||".
                            $mostrar[9]."||".
                            $mostrar[10]."||".
                            $mostrar[11];
                    ?>
                            <tr>
                                <td>
                                    <?php echo $mostrar[0] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[1] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[2] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[3] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[4] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[5] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[6] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[11] ?>
                                </td>
                                <td style="text-align: center;">
                                    <a class="btn btn-success" href="tcheck.php?id_stock_toner=<?php echo $mostrar[0];?>&serial_modelo=<?php echo $mostrar[1];?>&serial_cartucho=<?php echo $mostrar[2];?>&marca=<?php echo $mostrar[3];?>&color=<?php echo $mostrar[4];?>&fechaingreso=<?php echo $mostrar[5];?>&observacion=<?php echo $mostrar[6];?>&status_toner=<?php echo $mostrar[11];?>&codigoimpresora=<?php echo $impresoraid;?>">Revisiones Toner</a>
                                </td>
                                <td>
                                    <a class="btn btn-dark" href="sustituciontoner.php?id_stock_toner=<?php echo $mostrar[0];?>&serial_modelo=<?php echo $mostrar[1];?>&serial_cartucho=<?php echo $mostrar[2];?>&marca=<?php echo $mostrar[3];?>&color=<?php echo $mostrar[4];?>&fechaingreso=<?php echo $mostrar[5];?>&observacion=<?php echo $mostrar[6];?>&status_toner=<?php echo $mostrar[11];?>&codigoimpresora=<?php echo $impresoraid;?>">Sustituir</a>
                                </td>
                            </tr>
                            <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        <div class="col-md-12 text-center">
            <a class="btn btn-secondary" href="index.php">Volver</a>
        </div>
    </div>
    </div>

</body>
<script src="librerias/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="librerias/alertify/alertify.js"></script>
<script src="librerias/fontawesome/svg-with-js/js/fontawesome-all.js"></script>
<script src="js/index.js"></script>
<script src="js/selecttoner.js"></script>

</html>

<script type="text/javascript">
    $(document).ready(function () {
        $('#paginatoner').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                "infoEmpty": "Mostrando 0 to 0 of 0 Datos",
                "infoFiltered": "(Filtrado de _MAX_ total datos)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Datos",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    });
</script>