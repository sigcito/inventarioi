<?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();

    $sql = "SELECT o.id_operador, 
    o.nombre, 
    o.apellido, 
    o.cedula, 
    o.usuario, 
    r.descripcion FROM operadores o LEFT JOIN roles r ON o.id_rol = r.id_rol WHERE id_operador";

    $result = mysqli_query($conexion,$sql);
?>

        <div>
            <table id="operadores" class="table table-hover custom-table">
                <thead class="thead-dark">
                    <tr class="">
                        <th scope="col">#</th>
                        <th scope="col">ID Impresora</th>
                        <th scope="col">Fecha revision</th>
                        <th scope="col">Operador</th>
                        <th scope="col">Contador</th>
                        <th scope="col">Rol</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while ($mostrar=mysqli_fetch_row($result)){

                            $datosOp=$mostrar[0]."||".
                                    $mostrar[1]."||".
                                    $mostrar[2]."||".
                                    $mostrar[3]."||".
                                    $mostrar[4]."||".
                                    $mostrar[5];
                    ?>
                <tr>
                    <td><?php echo $mostrar[0] ?></td>
                    <td><?php echo $mostrar[1] ?></td>
                    <td><?php echo $mostrar[2] ?></td>
                    <td><?php echo $mostrar[3] ?></td>
                    <td><?php echo $mostrar[4] ?></td>
                    <td><?php echo $mostrar[5] ?></td>
                    <td style="text-align: center;">
                        <span class="btn btn-primary btn-sm" onclick="operadoradd('<?php echo $datosOp;?>')"> <i class="fas fa-pencil-alt fa-lg"></i></span>
                    </td>
                </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function () {
	$('#operadores').DataTable({
		"language": {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
			"infoEmpty": "Mostrando 0 to 0 of 0 Datoss",
			"infoFiltered": "(Filtrado de _MAX_ total datos)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Datos",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		}
    });

});
</script>