<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
    <title>APP IMPRESORAS</title>
</head>

<body>
    <!--CABECERA-->
    <div class="container">
        <h1 class="text-center mb-5 mt-4">Agregar Revision a impresora</h1>
    </div>
    <!--TABLA-->
    <div class="container-fluid">
        <div class="table-responsive">
            <?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();
    $impresoraid = $conexion->real_escape_string ($_GET ['datos']);

    $sql = "SELECT r.id_revision, 
    r.id_impresora, 
    r.f_ejecucion, 
    r.id_operador, 
    r.contador_impresora, 
    r.observaciones,
    o.nombre,
    o.apellido FROM revisiones r INNER JOIN impresora_fotocopiadora i ON i.cod_imp = r.id_impresora INNER JOIN operadores o ON o.id_operador = r.id_operador WHERE $impresoraid = r.id_impresora";

    $result = mysqli_query($conexion,$sql);
?>

            <div>
                <table id="paginatoner" class="table table-hover custom-table">
                    <thead class="thead-dark">
                        <tr class="">
                            <th scope="col">#</th>
                            <th scope="col">ID Impresora</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Operador</th>
                            <th scope="col">Contador</th>
                            <th scope="col">Observaciones</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($mostrar=mysqli_fetch_row($result)){
                            $datosIns=$mostrar[0]."||".
                            $mostrar[1]."||".
                            $mostrar[2]."||".
                            $mostrar[3]."||".
                            $mostrar[4]."||".
                            $mostrar[5]."||".
                            $mostrar[6]."||".
                            $mostrar[7];
                    ?>
                            <tr>
                                <td>
                                    <?php echo $mostrar[0] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[1] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[2] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[6] . " " .  $mostrar[7] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[4] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[5] ?>
                                </td>
                            </tr>
                            <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#revisionimp">Agregar revision</button>
            <a class="btn btn-secondary" href="index.php">Volver</a>
        </div>
    </div>
    </div>

<!--MODAL-->
<div class="modal fade" id="revisionimp" tabindex="-1" role="dialog" aria-labelledby="agregarImpresora" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <!--TABS-->
                        <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#registrarImpresora" role="tab" aria-controls="pills-home"
                                    aria-selected="true">Agregar revisión</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#agregarMarca" role="tab" aria-controls="pills-profile"
                                    aria-selected="false">Cargar Operador</a>
                            </li>
                        </ul>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="registrarImpresora" role="tabpanel" aria-labelledby="registrarImpresora-tab">
                                <!--TAB REGISTRAR IMPRESORA-->
                                <div class="container">
                                <h4 class="text-center">
                                Reporte Cartucho
                            </h4>
                            <form id="frmrevision">
                                <!-- Cartucho reporte -->
                                <div class="form-row form-group">
                                    <div class="col">
                                        <label for="">Id impresora check.</label>
                                        <input
                                            type="text"
                                            hidden=""
                                            id="impRev"
                                            name="impRev"
                                            value="<?php echo $impresoraid;?>">
                                        <input
                                            type="text"
                                            id=""
                                            name=""
                                            class="form-control"
                                            placeholder="<?php echo $impresoraid;?>"
                                            value=""
                                            disabled="disabled">
                                    </div>
                                    <div class="col">
                                        <label for="">Fecha ejecucion.</label>
                                        <input
                                            type="text"
                                            id="fechaRev"
                                            name="fechaRev"
                                            class="form-control"
                                            placeholder="Ingrese fecha de ejecucion">
                                    </div>
                                </div>
                                <div class="form-row form-group">
                                    <div class="col">
                                    <input
                                            type="text"
                                            id="operadorRev"
                                            name="operadorRev"
                                            class="form-control"
                                            hidden=""
                                            placeholder="Porcentaje toner">
                                        <label for="">Operador nombre.</label>
                                        <input
                                            type="text"
                                            id="operadorNombre"
                                            name="operadorNombre"
                                            class="form-control"
                                            placeholder="Dirijase a tab cargar operador" disabled>
                                    </div>
                                    <div class="col">
                                        <label for="">Operador apellido.</label>
                                        <input
                                            type="text"
                                            id="operadorApellido"
                                            name="operadorApellido"
                                            class="form-control"
                                            placeholder="Dirijase a tab cargar operador" disabled>
                                    </div>
                                </div>
                                <div class="form-row form-group">
                                    <div class="col">
                                        <label for="">Contador impresora.</label>
                                        <input
                                            type="text"
                                            id="contadorRev"
                                            name="contadorRev"
                                            class="form-control"
                                            placeholder="Ingrese contador de la impresora">
                                    </div>
                                </div>
                                <div class="form-row form-group">
                                    <div class="col">
                                        <label for="">Observación.</label>
                                        <textarea
                                            class="form-control"
                                            id="observacionRev"
                                            name="observacionRev"
                                            rows="3"
                                            placeholder="Ingresar Observación..."></textarea>
                                    </div>
                                </div>
                                <div class="float-right">
                                    <button type="button" id="guardarrevisionimp" class="btn btn-primary">Guardar</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                </div>
                            </form>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="agregarMarca" role="tabpanel" aria-labelledby="agregarMarca-tab">
                                <!--TAB AGREGAR MARCA-->
                                <div class="container">
                                    <h5 class="text-center">Agregar Detalles de Marca</h5>

                                    <div class="container-fluid">
                                        <div class="table-responsive">
                                            <div id="tabladataoperador"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="librerias/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="librerias/alertify/alertify.js"></script>
<script src="librerias/fontawesome/svg-with-js/js/fontawesome-all.js"></script>
<script src="js/index.js"></script>
<script src="js/selecttoner.js"></script>

</html>

<script type="text/javascript">
    $(document).ready(function () {
        $('#paginatoner').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                "infoEmpty": "Mostrando 0 to 0 of 0 Datoss",
                "infoFiltered": "(Filtrado de _MAX_ total datos)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Datos",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
    });
</script>