<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
    <title>APP IMPRESORAS</title>
</head>

<body>
    <!--CABECERA-->
    <div class="container">
        <h1 class="text-center mb-5 mt-4">Cartuchos Instalados</h1>
    </div>
    <!--TABLA-->
    <div class="container-fluid" id="refresh">
        <div class="table-responsive">
        <?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();

    $cod_toner = $conexion->real_escape_string ($_GET ['id_stock_toner']);
    $serial_modelo = $conexion->real_escape_string ($_GET ['serial_modelo']);
    $serial_cartucho = $conexion->real_escape_string ($_GET ['serial_cartucho']);
    $marca_toner = $conexion->real_escape_string ($_GET ['marca']);
    $color = $conexion->real_escape_string ($_GET ['color']);
    $fecha_ingreso = $conexion->real_escape_string ($_GET ['fechaingreso']);
    $observacion = $conexion->real_escape_string ($_GET ['observacion']);
    $status_condicion = $conexion->real_escape_string ($_GET ['status_toner']);
    $cod_impresora = $conexion->real_escape_string ($_GET ['codigoimpresora']);

    $sql = "SELECT tcheck.id_revision, 
    tcheck.porcentaje, 
    tcheck.contador, 
    acc.descripcion, 
    tcheck.observaciones FROM toner_check tcheck LEFT JOIN stock_toner st ON st.id_stock_toner = tcheck.id_toner_stock LEFT JOIN acciones acc ON acc.id_accion=tcheck.id_accion WHERE id_toner_stock=$cod_toner";

    $result = mysqli_query($conexion,$sql);
?>

        <div>
            <table id="revisiones" class="table table-hover custom-table">
                <thead class="thead-dark">
                    <tr class="">
                        <th scope="col">#</th>
                        <th scope="col">ID Impresora</th>
                        <th scope="col">Fecha revision</th>
                        <th scope="col">Operador</th>
                        <th scope="col">Contador</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while ($mostrar=mysqli_fetch_row($result)){

                          /*  $datos=$mostrar[0]."||".
                                    $mostrar[1]."||".
                                    $mostrar[2]."||".
                                    $mostrar[3]."||".
                                    $mostrar[4]."||".
                                    $mostrar[5]."||".
                                    $mostrar[6]."||".
                                    $mostrar[7]."||".
                                    $mostrar[8]."||".
                                    $mostrar[9]."||".
                                    $mostrar[10]."||".
                                    $mostrar[11]."||".
                                    $mostrar[12]."||".
                                    $mostrar[13]."||".
                                    $mostrar[14]."||".
                                    $mostrar[15];*/
                    ?>
                <tr>
                    <td><?php echo $mostrar[0] ?></td>
                    <td><?php echo $mostrar[1] ?></td>
                    <td><?php echo $mostrar[2] ?></td>
                    <td><?php echo $mostrar[3] ?></td>
                    <td><?php echo $mostrar[4] ?></td>
                </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tcheck">Agregar revision</button>
            <a class="btn btn-secondary" href="index.php">Volver al index</a>
        </div>
    </div>
    </div>

    <!-- Modal seleccionar toner instalado-->
<div class="modal fade" id="tcheck" tabindex="-2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <h4 class="text-center">
    Reporte Cartucho
</h4>
<form id="frmcheck">
    <!-- Cartucho reporte -->
    <div class="form-row form-group">
        <div class="col">
            <label for="">Id Toner instalado check.</label>
            <input
                type="text"
                hidden=""
                id="cod_tonerIns"
                name="cod_tonerIns"
                value="<?php echo $cod_toner;?>">
            <input
                type="text"
                id=""
                name=""
                class="form-control"
                placeholder="Id toner instalado"
                value="<?php echo $cod_toner;?>"
                disabled="disabled">
        </div>
        <div class="col">
            <label for="">Id Revision.</label>
            <input
                type="text"
                id="revisionIns"
                name="revisionIns"
                class="form-control"
                placeholder="Id revision">
        </div>
    </div>
    <div class="form-row form-group">
        <div class="col">
            <label for="">Porcentaje.</label>
            <input
                type="text"
                id="porcentajeIns"
                name="porcentajeIns"
                class="form-control"
                placeholder="Porcentaje toner">
        </div>
        <div class="col">
            <label for="">Contador.</label>
            <input
                type="text"
                id="contadorIns"
                name="contadorIns"
                class="form-control"
                placeholder="contador">
        </div>
    </div>
    <div class="form-row form-group">
        <div class="col">
            <label for="">Acción.</label>
            <select class="form-control" id="accionIns" name="accionIns">
                <option value="">Seleccionar</option>
            </select>
        </div>
    </div>
    <div class="form-row form-group">
        <div class="col">
            <label for="">Observación.</label>
            <textarea
                class="form-control"
                id="reporteobservacionIns"
                name="reporteobservacionIns"
                rows="3"
                placeholder="Ingresar Observación..."></textarea>
        </div>
    </div>
    <!--NUEVOS-->
    <div class="float-right">
        <button type="button" id="guardartcheck" class="btn btn-primary">Agregar revision</button>
        <button
            type="button"
            id="cerrar"
            data-dismiss="modal"
            class="btn btn-secondary">Cerrar</button>
    </div>
</form>
      </div>
    </div>
  </div>
</div>

</body>
<script src="librerias/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="librerias/alertify/alertify.js"></script>
<script src="librerias/fontawesome/svg-with-js/js/fontawesome-all.js"></script>
<script src="js/index.js"></script>
<script src="js/selecttoner.js"></script>

</html>

<script type="text/javascript">
    $(document).ready(function () {
        $('#revisiones').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                "infoEmpty": "Mostrando 0 to 0 of 0 Datoss",
                "infoFiltered": "(Filtrado de _MAX_ total datos)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Datos",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    });
</script>