<?php
    class crud{
        public function agregar($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into impresora_fotocopiadora (cod_tipo,cod_modelo,serial_imp,serialb_imp,status_imp,cod_ubicacion,observacion_imp) values ('$datos[0]','$datos[1]','$datos[2]','$datos[3]','$datos[4]','$datos[5]','$datos[6]') ";

            return mysqli_query($conexion,$sql);
        }
        
        public function tonerInsAStock($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            return $datostoner;
        }

        public function agregarmodelo($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into modelo_dispositivo (id_dispositivo,id_marca,des_modelo) values ('$datos[0]','$datos[1]','$datos[2]')";

            return mysqli_query($conexion,$sql);
        }

        public function agregarmarca($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into marca_dispositivo (des_marca) values ('$datos[0]')";

            return mysqli_query($conexion,$sql);
        }
        
        public function aggmantenimiento($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into mantenimientos (id_impresora,f_mantenimiento,id_operador,id_tipo_mant,id_empresa,observaciones) values ('$datos[0]','$datos[1]','$datos[2]','$datos[3]','$datos[4]','$datos[5]')";

            return mysqli_query($conexion,$sql);
        }

        public function aggempresa($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into empresas (nombre,rif,tlf_contacto) values ('$datos[0]','$datos[1]','$datos[2]')";
            return mysqli_query($conexion,$sql);
        }

        public function aggtecnicos($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into tecnicos (cedula,nombre,apellido,id_empresa) values ('$datos[0]','$datos[1]','$datos[2]','$datos[3]')";
            return mysqli_query($conexion,$sql);
        }

        public function aggtecnico_mantenimiento($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into tecnicos_mantenimiento (id_tecnico,id_mantenimiento) values ('$datos[0]','$datos[1]')";
            return mysqli_query($conexion,$sql);
        }

        public function aggrevisiones($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into revisiones (id_impresora,f_ejecucion,id_operador,contador_impresora,observaciones) values ('$datos[0]','$datos[1]','$datos[2]','$datos[3]','$datos[4]')";
            return mysqli_query($conexion,$sql);
        }

        public function aggoperador($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into operadores (nombre,apellido,cedula,secret,usuario,id_rol) values ('$datos[0]','$datos[1]','$datos[2]','$datos[3]','$datos[4]','$datos[5]')";
            return mysqli_query($conexion,$sql);
        }

        public function agregarmodelotoner($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into modelo_toner (serial_modelo,id_marca_toner,color) values ('$datos[0]','$datos[1]','$datos[2]')";
            return mysqli_query($conexion,$sql);
        }

        public function agregarstocktoner($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into stock_toner (cod_imp,serial_cartucho,f_ingreso,cod_modelo,id_status_condicion,id_status_stock,observacion) values ('$datos[0]','$datos[1]','$datos[2]','$datos[3]','$datos[4]','$datos[5]','$datos[6]')";

            return mysqli_query($conexion,$sql);
        }

        public function tonercheck($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql = "INSERT into toner_check (id_revision,id_toner_stock,porcentaje,contador,id_accion,observaciones) values ('$datos[0]','$datos[1]','$datos[2]','$datos[3]','$datos[4]','$datos[5]')";

            return mysqli_query($conexion,$sql);
        }

        public function agregarmarcatoner($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into marca_toner (descripcion) values ('$datos[0]')";

            return mysqli_query($conexion,$sql);
        }

        public function asociartonerstockimpresora($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into marca_toner (descripcion) values ('$datos[0]')";

            return mysqli_query($conexion,$sql);
        }

        public function agregarcompatibilidad($datos){
            $obj = new conectar();
            $conexion = $obj->conexion();

            $sql="INSERT into compatibilidad (id_modelo_impresora,id_modelo_toner) values ('$datos[0]','$datos[1]')";

            return mysqli_query($conexion,$sql);
        }
        
    }
?>