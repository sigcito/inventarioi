<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
    <title>APP IMPRESORAS</title>
</head>

<body>
    <!--CABECERA-->
    <div class="container">
        <h1 class="text-center mb-5 mt-4">Cartuchos Instalados</h1>
    </div>
     <?php
     require_once "clases/conexion.php";
     $obj = new conectar();
     $conexion = $obj->conexion();
     $cod_toner = $conexion->real_escape_string ($_GET ['id_stock_toner']);
     $serial_modelo = $conexion->real_escape_string ($_GET ['serial_modelo']);
     $serial_cartucho = $conexion->real_escape_string ($_GET ['serial_cartucho']);
     $marca_toner = $conexion->real_escape_string ($_GET ['marca']);
     $color = $conexion->real_escape_string ($_GET ['color']);
     $fecha_ingreso = $conexion->real_escape_string ($_GET ['fechaingreso']);
     $observacion = $conexion->real_escape_string ($_GET ['observacion']);
     $status_condicion = $conexion->real_escape_string ($_GET ['status_toner']);
     $cod_impresora = $conexion->real_escape_string ($_GET ['codigoimpresora']);
     ?>
                    <div class="container">
                        <h3 class="text-center">Sustitución de Cartuchos de Toner</h3>
                        <!--CARDS CARTUCHOS-TONNER-->
                            <div class="row">
                                <div class="col">
                                    <!--INSTALADOS-->
                                    <h4>Instalado</h4>
                                    <form id="frmsusttoner">
                                        <div class="form-row form-group">
                                        <input type="text" hidden="" id="cod_impresoraAsoc" name="cod_impresoraAsoc" value="<?php echo $cod_impresora;?>">
                                        <input type="text" hidden="" id="cod_tonerIns" name="cod_tonerIns" value="<?php echo $cod_toner;?>">
                                            <div class="col">
                                                <label for="">Serial Modelo.</label>
                                                <input type="text" id="serial_modelotonerIns" name="serial_modelotonerIns" class="form-control" placeholder="<?php echo $serial_modelo;?>" disabled>
                                            </div>
                                            <div class="col">
                                                <label for="">Serial Cartucho.</label>
                                                <input type="text" id="serial_cartuchotonerIns" name="serial_cartuchotonerIns" class="form-control" placeholder="<?php echo $serial_cartucho;?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Fecha Ingreso</label>
                                                <input type="text" id="fecha_ingresotonerIns" name="fecha_ingresotonerIns" class="form-control" placeholder="<?php echo $fecha_ingreso;?>" disabled>
                                            </div>
                                            <div class="col">
                                                <label for="">Status Stock</label>
                                                <input type="text"  id="" name="" class="form-control" placeholder="Toner a guardar." value="" disabled>
                                                <input type="text" hidden="" id="statusstockIns" name="statusstockIns" value="0">
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Status</label>
                                                <input type="text"  id="statusIns" name="statusIns" class="form-control" placeholder="<?php echo $status_condicion;?>" value="" disabled>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Observación</label>
                                                <textarea class="form-control" id="observacion_tonerIns" name="observacion_tonerIns" rows="3" placeholder="<?php echo $observacion;?>" disabled></textarea>
                                            </div>
                                        </div>

                                        <!-- <div class="col-md-12 text-center">
                                            <button type="button" class="btn btn-primary" id="ActuTonerStock" name="ActuTonerStock">Guardar</button>
                                        </div> -->

                                        <!-- Cartucho reporte -->
                                        <hr>
                                        <h4 class="text-center"> Reporte Cartucho </h4>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Id Toner instalado check.</label>
                                                <input type="text" id="idtonerInscheck" name="idtonerInscheck" class="form-control" placeholder="Id toner instalado" value="<?php echo $cod_toner?>" disabled>
                                            </div>
                                            <div class="col">
                                                <label for="">Id Revision.</label>
                                                <input type="text" id="revisionIns" name="revisionIns" class="form-control" placeholder="Id revision">
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                            <label for="">Porcentaje.</label>
                                                <input type="text" id="porcentajeIns" name="porcentajeIns" class="form-control" placeholder="Porcentaje toner">
                                            </div>
                                            <div class="col">
                                                <label for="">Contador.</label>
                                                <input type="text" id="contadorIns" name="contadorIns" class="form-control" placeholder="contador">
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                            <label for="">Acción.</label>
                                                <select class="form-control" id="accionIns" name="accionIns">
                                                    <option value="">Seleccionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Observación.</label>
                                                <textarea class="form-control" id="reporteobservacionIns" name="reporteobservacionIns" rows="3" placeholder="Ingresar Observación..."></textarea>
                                            </div>
                                        </div>
                                        <!--NUEVOS-->
                                        <hr>
                                        <h4 style="text-align: center;">Nuevos</h4>
                                        <div class="form-row form-group">
                                            <div class="col">
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#listartonersasust">Buscar toner a sustituir.</button>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                        <input type="text" hidden="" id="cod_tonerNew" name="cod_tonerNew" value="">
                                            <div class="col">
                                                <label for="">Serial Modelo</label>
                                                <input type="text" id="serial_modelotonerNew" name="serial_modelotonerNew" class="form-control" placeholder="Serial Modelo" disabled>
                                            </div>
                                            <div class="col">
                                                <label for="">Serial Cartucho</label>
                                                <input type="text" id="serial_cartuchotonerNew" name="serial_cartuchotonerNew" class="form-control" placeholder="Serial cartucho" disabled>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Fecha Ingreso</label>
                                                <input type="text" id="fecha_ingresotonerNew" name="fecha_ingresotonerNew" class="form-control" placeholder="Fecha ingreso" disabled>
                                            </div>
                                            <div class="col">
                                                <label for="">Status</label>
                                                <select class="form-control" id="statusNew" name="statusNew" disabled>
                                                    <option value="">Seleccionar.</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                        <div class="col">
                                                <label for="">Status Stock</label>
                                                <input type="text" id="" name="" class="form-control" placeholder="Toner a estado instalado." disabled>
                                                <input type="text" hidden="" id="statusstockNew" name="statusstockNew" value="1">
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Observación</label>
                                                <textarea class="form-control" id="observacion_tonerNew" name="observacion_tonerNew" rows="3" placeholder="Ingresar Observación..." disabled></textarea>
                                            </div>
                                        </div>
                                        <div class="float-right">
                                            <button type="button" id="SustituirToner" class="btn btn-primary">Sustituir Toner</button>
                                            <a class="btn btn-dark" href="index.php">Volver al index</a>
                                        </div>
                                    </form>
                        </div>
                    </div>
                </div>


    <!-- Modal seleccionar toner instalado-->
<div class="modal fade" id="listartonersasust" tabindex="-2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="text-center" id="exampleModalLabel"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    <div class="container-fluid">
    <h3 class="text-center" id="exampleModalLabel">Cargar toner nuevo.</h3>
        <div class="table-responsive">
        <?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();

    $sql = "SELECT st.id_stock_toner,   /* 0 */
    mt.serial_modelo,                   /* 1 */
    st.serial_cartucho,                 /* 2 */
    mat.descripcion,                    /* 3 */
    mt.color,                           /* 4 */
    st.f_ingreso,                       /* 5 */
    st.observacion,                     /* 6 */
    id_status_condicion,                /* 7 */
    id_status_stock,                    /* 8 */
    st.cod_imp,                         /* 9 */
    st.cod_modelo,                      /* 10 */
    stat.des_status_toner               /* 11 */
    FROM stock_toner st LEFT JOIN modelo_toner mt ON mt.id_modelo_toner = st.cod_modelo LEFT JOIN marca_toner mat ON mat.id_marca_toner = mt.id_marca_toner LEFT JOIN status_toner stat ON stat.cod_statustoner = st.id_status_condicion WHERE id_status_stock=0 AND mt.color='$color'";

    $result = mysqli_query($conexion,$sql);
?>

            <div>
                <table id="paginatoner" class="table table-hover custom-table">
                    <thead class="thead-dark">
                        <tr class="">
                            <th scope="col">#</th>
                            <th scope="col">Serial</th>
                            <th scope="col">Serial cartucho</th>
                            <th scope="col">Marca</th>
                            <th scope="col">Color</th>
                            <th scope="col">Status</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($mostrar=mysqli_fetch_row($result)){
                            $datosIns=$mostrar[0]."||".
                            $mostrar[1]."||".
                            $mostrar[2]."||".
                            $mostrar[3]."||".
                            $mostrar[4]."||".
                            $mostrar[5]."||".
                            $mostrar[6]."||".
                            $mostrar[7]."||".
                            $mostrar[8]."||".
                            $mostrar[9]."||".
                            $mostrar[10]."||".
                            $mostrar[11];
                    ?>
                            <tr>
                                <td>
                                    <?php echo $mostrar[0] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[1] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[2] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[3] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[4] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[5] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[6] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[11] ?>
                                </td>
                                <td style="text-align: center;">
                                    <span class="btn btn-primary btn-sm" data-dismiss="modal" onclick="agregaformtoner('<?php echo $datosIns;?>')"> <i class="fas fa-pencil-alt fa-lg"></i></span>
                                </td>
                            </tr>
                            <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>
</div>

</body>
<script src="librerias/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="librerias/alertify/alertify.js"></script>
<script src="librerias/fontawesome/svg-with-js/js/fontawesome-all.js"></script>
<script src="js/index.js"></script>
<script src="js/selecttoner.js"></script>

</html>

<script type="text/javascript">
    $(document).ready(function () {
        $('#paginatoner').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "No hay toners disponibles para sustituir.",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                "infoEmpty": "Mostrando 0 to 0 of 0 Datos",
                "infoFiltered": "(Filtrado de _MAX_ total datos)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Datos",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    });
</script>