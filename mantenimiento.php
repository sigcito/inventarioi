<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
    <title>APP IMPRESORAS</title>
</head>

<body>
    <!--CABECERA-->
    <div class="container">
        <h1 class="text-center mb-5 mt-4">Mantenimientos.</h1>
    </div>
    <div class="container">
        <nav class="navbar justify-content-between float-left">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mantenimiento">Agregar mantenimiento / Operadores</button>
            <a class="btn btn-dark ml-4" href="index.php">Volver a index.</a>
        </nav>
        <!--MODAL MANTENIMIENTO-->
        <div class="modal fade" id="mantenimiento" tabindex="-1" role="dialog" aria-labelledby="mantenimiento" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <!--TABS MANTENIMIENTO-->
                        <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#agregarMantenimiento" role="tab" aria-controls="pills-home"
                                    aria-selected="true">Mantenimiento</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#agregarTecnico" role="tab" aria-controls="pills-profile"
                                    aria-selected="false">Agregar Tecnico</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#agregarEmpresa" role="tab" aria-controls="pills-contact"
                                    aria-selected="false">Agregar Empresa</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#agregarOperador" role="tab" aria-controls="pills-contact"
                                    aria-selected="false">Agregar Operador</a>
                            </li>
                        </ul>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="agregarMantenimiento" role="tabpanel" aria-labelledby="mantenimiento-tab">
                                <!--TAB MANTENIMIENTO-->
                                <div class="container">
                                    <h5 class="text-center">Mantenimiento</h5>
                                    <form id="frmaddmantenimiento">
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Impresora</label>
                                                <input type="text" class="form-control" placeholder="Cedula" id="id_impresoraid" hidden name="id_impresoraid">
                                                <input type="text" class="form-control" id="id_impresoraSer" name="id_impresoraSer" placeholder="Cargue la impresora." disabled>
                                            </div>
                                            <div class="col">
                                                <label for="">Fecha de mantenimiento</label>
                                                <input type="text" class="form-control date" placeholder="Fecha" id="f_mantenimiento" name="f_mantenimiento">
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Operador</label>
                                                <select class="form-control" id="id_operador" name="id_operador">
                                                <option value="">Seleccionar</option>
                                            </select>
                                            </div>
                                            <div class="col">
                                                <label for="">Tipo de mantenimiento</label>
                                                <select class="form-control" id="id_tipo_mant" name="id_tipo_mant">
                                                <option value="">Seleccionar</option>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col-md">
                                                <label for="">Empresa</label>
                                                <select class="form-control" id="id_empresa" name="id_empresa" placeholder="Empresa">
                                                <option value="">Seleccionar</option>
                                            </select>
                                            </div>
                                            <div class="col-md">
                                                <label for="">Tecnico</label>
                                                <select class="form-control" id="id_tecnico" name="id_tecnico">
                                                <option value="">Seleccionar</option>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Observación</label>
                                                <textarea class="form-control" id="observacion_mant" name="observacion_mant" rows="3" placeholder="Ingresar Observación..."></textarea>
                                            </div>
                                        </div>
                                        <div class="float-right mb-4">
                                            <button type="button" id="guardarmantenimiento" class="btn btn-primary">Guardar</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        </div>
                                        <div class="container-fluid"><hr></div>
                                        <div class="container-fluid">
                                            <div class="table-responsive">
                                            <h5 class="text-center">Cargar impresora</h5>
                                            <div id="cargarimp"></div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="agregarTecnico" role="tabpanel" aria-labelledby="agregarTecnico-tab">
                                <!--TAB AGREGAR TECNICO-->
                                <div class="container">
                                    <h5 class="text-center">Agregar Tecnico</h5>
                                    <form id="frmaddtecnico">
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Cedula</label>
                                                <input type="text" class="form-control" placeholder="Cedula" id="cedula_tecnico" name="cedula_tecnico">
                                            </div>
                                            <div class="col">
                                                <label for="">Nombre</label>
                                                <input type="text" class="form-control" placeholder="Nombre" id="nombre_tecnico" name="nombre_tecnico">
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col">
                                                <label for="">Apellido</label>
                                                <input type="text" class="form-control" placeholder="Apellido" id="apellido_tecnico" name="apellido_tecnico">
                                            </div>
                                            <div class="col">
                                                <label for="">Empresa</label>
                                                <select class="form-control" id="id_empresatec" name="id_empresatec">
                                                <option value="">Seleccionar</option>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="float-right">
                                            <button type="button" class="btn btn-primary" id="guardartecnico">Guardar</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="agregarEmpresa" role="tabpanel" aria-labelledby="agregarTecnico-tab">
                                <!--TAB AGREGAR EMPRESA-->
                                <div class="container">
                                    <h5 class="text-center">Agregar Empresa</h5>
                                    <form id="frmaddempresa">
                                        <div class="form-row form-group">
                                            <div class="col-sm-6 m-auto">
                                                <label for="">Nombre</label>
                                                <input type="text" class="form-control" id="nombre_empresa" name="nombre_empresa" placeholder="Nombre">
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col-sm-6 m-auto">
                                                <label for="">Rif</label>
                                                <input type="text" class="form-control" id="rif_empresa" name="rif_empresa" placeholder="Rif">
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col-sm-6 m-auto">
                                                <label for="">Telefono de contacto</label>
                                                <input type="text" class="form-control" id="telf_contactoem" name="telf_contactoem" placeholder="Telefono de contacto">
                                            </div>
                                        </div>
                                        <div class="float-right">
                                            <button type="button" class="btn btn-primary" id="guardarempresa">Guardar</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="agregarOperador" role="tabpanel" aria-labelledby="agregarTecnico-tab">
                                <!--TAB AGREGAR OPERADOR-->
                                <div class="container">
                                    <h5 class="text-center">Agregar Operador</h5>
                                    <form id="frmaddoperador">
                                        <div class="form-row form-group">
                                            <div class="col-sm-6 m-auto">
                                                <label for="">Nombre</label>
                                                <input type="text" class="form-control" id="nombreOp" name="nombreOp" placeholder="Ingrese el nombre del operador">
                                            </div>
                                            <div class="col-sm-6 m-auto">
                                                <label for="">Apellido</label>
                                                <input type="text" class="form-control" id="apellidoOp" name="apellidoOp" placeholder="Ingrese el apellido del operador">
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col-sm-6 m-auto">
                                                <label for="">Cedula</label>
                                                <input type="text" class="form-control" id="cedulaOp" name="cedulaOp" placeholder="Ingrese la cedula del operador">
                                            </div>
                                            <div class="col-sm-6 m-auto">
                                                <label for="">Contraseña</label>
                                                <input type="password" class="form-control" id="secretOp" name="secretOp" placeholder="Ingrese la contraseña del operador">
                                            </div>
                                        </div>
                                        <div class="form-row form-group">
                                            <div class="col-sm-6 m-auto">
                                                <label for="">Usuario</label>
                                                <input type="text" class="form-control" id="usuarioOp" name="usuarioOp" placeholder="Ingrese el usuario del operador">
                                            </div>
                                            <div class="col">
                                                <label for="">Rol</label>
                                                <select class="form-control" id="id_rol" name="id_rol">
                                                <option value="">Seleccionar</option>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="float-right">
                                            <button type="button" class="btn btn-primary" id="guardaroperador">Guardar</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--TABLA-->

        <div class="container-fluid">
            <div class="table-responsive">
                <div id="tablamantenimientos"></div>
            </div>
        </div>

</body>
<script src="librerias/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="librerias/alertify/alertify.js"></script>
<script src="librerias/fontawesome/svg-with-js/js/fontawesome-all.js"></script>
<script src="js/index.js"></script>
<script src="js/selecttoner.js"></script>

</html>