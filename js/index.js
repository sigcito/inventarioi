var cedula = /^[0-9]{7,8}$/;
var porcentaje = /^[0-9]{0,3}$/;
var contador = /^[0-9]{0,8}$/;

$(document).ready(function () {
    $('#tabladatatable').load('tablaimpres.php');
});

$(document).ready(function () {
    $('#tabladatatonerasoc').load('tablatonerasociado.php');
});

$(document).ready(function () {
    $('#tablamantenimientos').load('tablamantenimientos.php');
});

$(document).ready(function () {
    $('#tablarevisiones').load('tablarevisiones.php');
});

$(document).ready(function () {
    $('#tabladataoperador').load('tablaoperadores.php');
});

$(document).ready(function () {
    $('#tabladatamodeloimp').load('tablamodeloimp.php');
});

$(document).ready(function () {
    $('#tabladatamodelotoner').load('tablamodelotoner.php');
});

$(document).ready(function () {
    $('#tablatoners').load('tablatoner.php');
});

$(document).ready(function () {
    $('#cargarimp').load('tablacargarimp.php');
});

$(document).ready(function () {
    $('#cargarestaciones').load('estaciones.php');
});

$(document).ready(function () {
    $('#tabladatapc').load('tabladatapc.php');
});

$(document).ready(function () {
    $('#tabladatamonitor').load('tablamonitor.php');
});

$(document).ready(function () {

    /* SELECT CARGAR STATUS */

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_status.php',
        })
        .done(function (status) {
            $('#status_imp').html(status)
        })
        .fail(function () {
            alert('Error al cargar status')
        })

    /*SELECT DEPENDIENTE EDIFICIO UBICACION*/

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_edificio.php',
        })
        .done(function (edificio) {
            $('#des_edificio').html(edificio)
        })
        .fail(function () {
            alert('Error al cargar edificios')
        })

    $('#des_edificio').on('change', function () {
        var id_edificio = $('#des_edificio').val();
        $.ajax({
                type: 'POST',
                url: 'procesos/cargar_ubicacion.php',
                data: {
                    'id_edificio': id_edificio
                }
            })
            .done(function (edificio) {
                $('#des_ubicacion').html(edificio)
            })
            .fail(function () {
                alert('Error al cargar ubicación')
            })
    })

    /* SELECT DEPENDIENTE TIPO and MARCA -> MODELO*/

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_tipo.php',
        })
        .done(function (tipoimp) {
            $('#des_tipo').html(tipoimp)
        })
        .fail(function () {
            alert('Error al cargar tipos')
        })

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_marca.php',
        })
        .done(function (tipoimp) {
            $('#des_marca').html(tipoimp)
        })
        .fail(function () {
            alert('Error al cargar marcas')
        })

    $('#des_marca').on('change', function () {
        var id_dispositivo = $('#des_tipo').val();
        var id_marca = $('#des_marca').val();
        $.ajax({
                type: 'POST',
                url: 'procesos/cargar_modelo.php',
                data: {
                    'id_dispositivo': id_dispositivo,
                    'id_marca': id_marca
                }
            })
            .done(function (tipoimp) {
                $('#des_modelo').html(tipoimp)
            })
            .fail(function () {
                alert('Error al cargar modelos')
            })
    })

    $('#des_tipo').on('change', function () {
        var id_dispositivo = $('#des_tipo').val();
        var id_marca = $('#des_marca').val();
        $.ajax({
                type: 'POST',
                url: 'procesos/cargar_modelo.php',
                data: {
                    'id_dispositivo': id_dispositivo,
                    'id_marca': id_marca
                }
            })
            .done(function (tipoimp) {
                $('#des_modelo').html(tipoimp)
            })
            .fail(function () {
                alert('Error al cargar modelos')
            })
    })

    /* TAB AGREGAR MODELO SELECTS */

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_tipo.php',
        })
        .done(function (tipoimp) {
            $('#des_tipomodelo').html(tipoimp)
        })
        .fail(function () {
            alert('Error al cargar tipos')
        })

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_marca.php',
        })
        .done(function (tipoimp) {
            $('#des_marcamodelo').html(tipoimp)
        })
        .fail(function () {
            alert('Error al cargar marcas')
        })


})

/* SELECTS ACTUALIZAR IMPRESORA */

$(document).ready(function () {

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_status.php',
        })
        .done(function (status) {
            $('#status_impActu').html(status)
        })
        .fail(function () {
            alert('Error al cargar status')
        })

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_tipoimp.php',
        })
        .done(function (status) {
            $('#des_tipomarcaActu').html(status)
        })
        .fail(function () {
            alert('Error al cargar status')
        })

    /*SELECT DEPENDIENTE EDIFICIO UBICACION*/

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_edificio.php',
        })
        .done(function (edificio) {
            $('#des_edificioActu').html(edificio)
        })
        .fail(function () {
            alert('Error al cargar edificios')
        })

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_ubicacionActu.php',
        })
        .done(function (edificio) {
            $('#des_ubicacionActu').html(edificio)
        })
        .fail(function () {
            alert('Error al cargar edificios')
        })

    /*$('#des_edificioActu').on('change', function(){
        var id_edificio = $('#des_edificioActu').val();
          $.ajax({
                type: 'POST',
                url: 'procesos/cargar_ubicacion.php',
                data: {'id_edificio': id_edificio}
            })
            .done(function(edificio){
                $('#des_ubicacionActu').html(edificio)
            })
            .fail(function(){
                alert('Error al cargar ubicacion')
            })  
    })*/

    /* SELECT DEPENDIENTE TIPO and MARCA -> MODELO  ACTUALIZAR IMPRESORA */

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_tipo.php',
        })
        .done(function (tipoimp) {
            $('#des_tipoActu').html(tipoimp)
        })
        .fail(function () {
            alert('Error al cargar tipos')
        })

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_marca.php',
        })
        .done(function (tipoimp) {
            $('#des_marcaActu').html(tipoimp)
        })
        .fail(function () {
            alert('Error al cargar marcas')
        })

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_modeloActu.php',
        })
        .done(function (tipoimp) {
            $('#des_modeloActu').html(tipoimp)
        })
        .fail(function () {
            alert('Error al cargar marcas')
        })

    /*$('#des_marcaActu').on('change', function(){
        var id_dispositivo = $('#des_tipoActu').val();
        var id_marca = $('#des_marcaActu').val();
          $.ajax({
                type: 'POST',
                url: 'procesos/cargar_modelo.php',
                data: {'id_dispositivo': id_dispositivo, 'id_marca': id_marca}
            })
            .done(function(tipoimp){
                $('#des_modeloActu').html(tipoimp)
            })
            .fail(function(){
                alert('Error al cargar modelos')
            })  
    })

    $('#des_tipoActu').on('change', function(){
        var id_dispositivo = $('#des_tipoActu').val();
        var id_marca = $('#des_marcaActu').val();
          $.ajax({
                type: 'POST',
                url: 'procesos/cargar_modelo.php',
                data: {'id_dispositivo': id_dispositivo, 'id_marca': id_marca}
            })
            .done(function(tipoimp){
                $('#des_modeloActu').html(tipoimp)
            })
            .fail(function(){
                alert('Error al cargar modelos')
            })  
    })*/

})

/* SELECTS TAB TONERS STOCK */

$(document).ready(function () {
    /* SELECTS MODELO TONERS */
    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_marcatoner.php',
        })
        .done(function (marcastock) {
            $('#cod_marcaStock').html(marcastock)
        })
        .fail(function () {
            alert('Error al cargar marca de toner.')
        })

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_marcatoner.php',
        })
        .done(function (marcastock) {
            $('#cod_marcaStockActu').html(marcastock)
        })
        .fail(function () {
            alert('Error al cargar marca de toner.')
        })

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_marcatoner.php',
        })
        .done(function (marcasmodelo) {
            $('#id_marcaToner').html(marcasmodelo)
        })
        .fail(function () {
            alert('Error al cargar marca de toner.')
        })
})

$('#cod_marcaStock').on('change', function () {
    var cod_marca = $('#cod_marcaStock').val();
    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_modelotoner.php',
            data: {
                'cod_marca': cod_marca
            }
        })
        .done(function (marcastock) {
            $('#cod_modeloStock').html(marcastock)
        })
        .fail(function () {
            alert('Error al cargar select modelo stock')
        })
})

$.ajax({
        type: 'POST',
        url: 'procesos/cargar_modelotoneractu.php',
    })
    .done(function (marcastock) {
        $('#cod_modeloStockActu').html(marcastock)
    })
    .fail(function () {
        alert('Error al cargar select modelo stockACTU')
    })

$.ajax({
        type: 'POST',
        url: 'procesos/cargar_statustoner.php',
    })
    .done(function (status) {
        $('#status_condicionStock').html(status)
    })
    .fail(function () {
        alert('Error al cargar status condicion toner')
    })

$.ajax({
        type: 'POST',
        url: 'procesos/cargar_statustoner.php',
    })
    .done(function (status) {
        $('#status_condicionStockActu').html(status)
    })
    .fail(function () {
        alert('Error al cargar status condicion toner')
    })

/* SELECTS MODULO MANTENIMIENTO */

$(document).ready(function () {

    /* SELECT TIPO MANTENIMIENTO */

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_tipomant.php',
        })
        .done(function (tipomant) {
            $('#id_tipo_mant').html(tipomant)
        })
        .fail(function () {
            alert('Error al cargar tipos de mantenimientos.')
        })

        /* SELECT ROL OPERADOR */

        $.ajax({
            type: 'POST',
            url: 'procesos/cargar_roles.php',
        })
        .done(function (rol) {
            $('#id_rol').html(rol)
        })
        .fail(function () {
            alert('Error al cargar roles operador.')
        })


    /* SELECT DEPENDIENTE EMPRESA - TECNICO */

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_empresa.php',
        })
        .done(function (empresa) {
            $('#id_empresa').html(empresa)
        })
        .fail(function () {
            alert('Error al cargar empresa')
        })

    $('#id_empresa').on('change', function () {
        var id_empresa = $('#id_empresa').val();
        $.ajax({
                type: 'POST',
                url: 'procesos/cargar_tecnico.php',
                data: {
                    'id_empresa': id_empresa
                }
            })
            .done(function (empresa) {
                $('#id_tecnico').html(empresa)
            })
            .fail(function () {
                alert('Error al cargar tecnicos.')
            })
    })

    /* SELECT IMPRESORA */

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_imp.php',
        })
        .done(function (impresora) {
            $('#id_impresora').html(impresora)
        })
        .fail(function () {
            alert('Error al cargar impresora.')
        })

    /* SELECT OPERADOR */

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_operador.php',
        })
        .done(function (operador) {
            $('#id_operador').html(operador)
        })
        .fail(function () {
            alert('Error al cargar operadores.')
        })

    /* SELECT EMPRESA TAB TECNICO */

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_empresa.php',
        })
        .done(function (empresa) {
            $('#id_empresatec').html(empresa)
        })
        .fail(function () {
            alert('Error al cargar empresa')
        })

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_toner.php',
        })
        .done(function (status) {
            $('#error').html(status)
        })
        .fail(function () {
            alert('Error al cargar status')
        })

    $.ajax({
            type: 'POST',
            url: 'procesos/cargar_sta_stock.php',
        })
        .done(function (status) {
            $('#error1').html(status)
        })
        .fail(function () {
            alert('Error al cargar status')
        })
})

/* TRIGGER BOTON ADD IMPRESORA */

$(document).ready(function () {
    $('#guardarcambios').click(function () {
        /* Validacion campos */

        var des_tipo = $("#des_tipo").val();
        var des_marca = $("#des_marca").val();
        var des_modelo = $("#des_modelo").val();
        var serial_imp = $("#serial_imp").val();
        var serialb_imp = $("#serialb_imp").val();
        var des_edificio = $("#des_edificio").val();
        var des_ubicacion = $("#des_ubicacion").val();
        var observacion_imp = $("#observacion_imp").val();

        if (des_tipo == "") {
            alertify.error("Seleccione el tipo de dispositivo impresora/fotocopiadora - impresora.");
            return false;
        } else {
            if (des_marca == "") {
                alertify.error("Seleccione la marca de la impresora.");
                return false;
            } else {
                if (des_modelo == "") {
                    alertify.error("Seleccione el modelo de la impresora.");
                    return false;
                } else {
                    if (serial_imp == "") {
                        alertify.error("Introduzca el serial de la impresora.");
                        return false;
                    } else {
                        if (serialb_imp == "") {
                            alertify.error("Introduzca el serial bien nacional de la impresora.");
                            return false;
                        } else {
                            if (des_edificio == "") {
                                alertify.error("Seleccione el edificio de la impresora.");
                                return false;
                            } else {
                                if (des_ubicacion == "") {
                                    alertify.error("Seleccione la ubicacion de la impresora.");
                                    return false;
                                } else {
                                    if (observacion_imp == "") {
                                        alertify.error("Introduzca las observaciones de la impresora.");
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        datos = $('#frmnuevo').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregar.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmnuevo')[0].reset();
                    $('#tabladatatable').load('tablaimpres.php');
                    alertify.success("Impresora agregada con exito.");
                } else {
                    alertify.error("Fallo al agregar impresora :(");
                }
            }
        });
    });
});

/* TRIGGER BOTON ADD MARCA */

$(document).ready(function () {
    $('#guardarcambiosmarca').click(function () {
        /* Validacion campos */

        var des_marcamarca = $("#des_marcamarca").val();

        if (des_marcamarca == "") {
            alertify.error("Introduzca la nueva marca de impresoras.");
            return false;
        } else {}
        datos = $('#frmnuevamarca').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregarmarcaimpresora.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmnuevamarca')[0].reset();
                    $('#tabladatatable').load('tablaimpres.php');
                    alertify.success("Marca agregada con exito.");
                } else {
                    alertify.error("Fallo al agregar marca :(");
                }
            }
        });
    });
});

/* TRIGGER BOTON ADD MODELO */

$(document).ready(function () {
    $('#guardarcambiosmodelo').click(function () {
        /* Validaciones */

        var des_tipomodelo = $("#des_tipomodelo").val();
        var des_marcamodelo = $("#des_marcamodelo").val();
        var des_modelomodelo = $("#des_modelomodelo").val();

        if (des_tipomodelo == "") {
            alertify.error("Seleccione el tipo de dispositivo.");
            return false;
        } else {
            if (des_marcamodelo == "") {
                alertify.error("Seleccione la marca a ser asociada al modelo.");
                return false;
            } else {
                if (des_modelomodelo == "") {
                    alertify.error("Introduzca el modelo nuevo.");
                    return false;
                } else {}
            }
        }

        datos = $('#frmnuevomodelo').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregarmodeloimpresora.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmnuevomodelo')[0].reset();
                    $('#tabladatatable').load('tablaimpres.php');
                    alertify.success("Modelo agregado con exito.");
                } else {
                    alertify.error("Fallo al agregar modelo :(");
                }
            }
        });
    });
});

/*TRIGGER BOTON AGREGAR COMPATIBILIDAD*/

$(document).ready(function () {
    $('#guardarcompatibilidad').click(function () {

        var modeloimpid = $("#modeloimpid").val();
        var modelotid = $("#modelotid").val();
        // --- Condicionales anidados ----

        if (modeloimpid == "") {
            alertify.error("Seleccione la impresora para agregar compatibilidad.");
            return false;
        } else {

            if (modelotid == "") {
                alertify.error("Seleccione el toner para agregar compatibilidad.");
                return false;
            } else {


            }
        }
        datos = $('#frmcompatibilidad').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregarcompatibilidad.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmcompatibilidad')[0].reset();
                    $('#tabladatatable').load('tablaimpres.php');
                    alertify.success("Compatibilidad agregada con exito.");
                } else {
                    alertify.error("Fallo al agregar Compatibilidad :(");
                }
            }
        });
    });
});

/*TRIGGER BOTON AGREGAR MANTENIMIENTO*/

$(document).ready(function () {
    $('#guardarmantenimiento').click(function () {
        /* Validacion campos */

        var id_impresoraid = $("#id_impresoraid").val();
        var f_mantenimiento = $("#f_mantenimiento").val();
        var id_operador = $("#id_operador").val();
        var id_tipo_mant = $("#id_tipo_mant").val();
        var id_empresa = $("#id_empresa").val();
        var id_tecnico = $("#id_tecnico").val();
        var observacion_mant = $("#observacion_mant").val();

        if (id_impresoraid == "") {
            alertify.error("Cargue la impresora a mantenimiento.");
            return false;
        } else {
            if (f_mantenimiento == "") {
                alertify.error("Ingrese la fecha del mantenimiento.");
                return false;
            } else {
                if (id_operador == "") {
                    alertify.error("Seleccione el operador.");
                    return false;
                } else {
                    if (id_tipo_mant == "") {
                        alertify.error("Seleccione el tipo de mantenimiento a realizar.");
                        return false;
                    } else {
                        if (id_empresa == "") {
                            alertify.error("Seleccione la empresa.");
                            return false;
                        } else {
                            if (id_tecnico == "") {
                                alertify.error("Seleccione el tecnico");
                                return false;
                            } else {
                                if (observacion_mant == "") {
                                    alertify.error("Ingrese las observaciones del mantenimiento.");
                                    return false;
                                } else {}
                            }
                        }
                    }
                }
            }
        }
        datos = $('#frmaddmantenimiento').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregarmantenimiento.php",
            success: function (r) {
                if (r == 1) {
                    console.log(datos);
                    $('#frmaddmantenimiento')[0].reset();
                    $('#tabladatatable').load('tablaimpres.php');
                    alertify.success("Mantenimiento agregado con exito.");
                } else {
                    console.log(datos);
                    alertify.error("Fallo al agregar mantenimiento :(");
                }
            }
        });
    });
});

/* TRIGGER BOTON ADD TECNICO */

$(document).ready(function () {
    $('#guardartecnico').click(function () {

        var cedula_tecnico = $("#cedula_tecnico").val();
        var nombre_tecnico = $("#nombre_tecnico").val();
        var apellido_tecnico = $("#apellido_tecnico").val();
        var id_empresatec = $("#id_empresatec").val();

        if (cedula_tecnico == "" || !cedula.test(cedula_tecnico)) {
            alertify.error("Ingrese la cedula del tecnico, Debe tener 7 - 8 caracteres.");
            return false;
        } else {
            if (nombre_tecnico == "") {
                alertify.error("Ingrese el nombre del tecnico.");
                return false;
            } else {
                if (apellido_tecnico == "") {
                    alertify.error("Ingrese el apellido del tecnico.");
                    return false;
                } else {
                    if (id_empresatec == "") {
                        alertify.error("Seleccione la empresa.");
                        return false;
                    }
                }
            }
        }
        datos = $('#frmaddtecnico').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregartecnico.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmaddtecnico')[0].reset();
                    $('#tabladatamantenimiento').load('tablamant.php');
                    alertify.success("Tecnico agregado con exito.");
                } else {
                    alertify.error("Fallo al agregar tecnico :(");
                }
            }
        });
    });
});

/* TRIGGER BOTON ASOCIAR TECNICO A MANTENIMIENTO*/

$(document).ready(function () {
    $('#guardartecnico_mantenimiento').click(function () {
        datos = $('#frmaddtecnico_mantenimiento').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregartec_mant.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmaddtecnico_mantenimiento')[0].reset();
                    $('#tabladatamantenimiento').load('tablamant.php');
                    alertify.success("Tecnico asociado a mantenimiento con exito.");
                } else {
                    alertify.error("Fallo al asociar tecnico :(");
                }
            }
        });
    });
});

/* TRIGGER BOTON ADD EMPRESA */

$(document).ready(function () {
    $('#guardarempresa').click(function () {
        var nombre_empresa = $("#nombre_empresa").val();
        var rif_empresa = $("#rif_empresa").val();
        var telf_contactoem = $("#telf_contactoem").val();

        if (nombre_empresa == "") {
            alertify.error("Ingrese el nombre de la empresa.");
            return false;
        } else {
            if (rif_empresa == "") {
                alertify.error("Ingrese el rif de la empresa.");
                return false;
            } else {
                if (telf_contactoem == "") {
                    alertify.error("Ingrese el telefono de contacto.");
                    return false;
                }
            }
        }
        datos = $('#frmaddempresa').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregarempresa.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmaddempresa')[0].reset();
                    $('#tabladatamantenimiento').load('tablamant.php');
                    alertify.success("Empresa agregada con exito.");
                } else {
                    alertify.error("Fallo al agregar empresa :(");
                }
            }
        });
    });
});

/* TRIGGER BOTON AGREGAR OPERADOR */

$(document).ready(function () {
    $('#guardaroperador').click(function () {
        var nombreOp = $("#nombreOp").val();
        var apellidoOp = $("#apellidoOp").val();
        var cedulaOp = $("#cedulaOp").val();
        var secretOp = $("#secretOp").val();
        var usuarioOp = $("#usuarioOp").val();
        var id_rol = $("#id_rol").val();

        if (nombreOp == "") {
            alertify.error("Ingrese el nombre del operador.");
            return false;
        } else {
            if (apellidoOp == "") {
                alertify.error("Ingrese el apellido del operador.");
                return false;
            } else {
                if (cedulaOp == "" || !cedula.test(cedulaOp)) {
                    alertify.error("Ingrese la cedula del operador (solo numeros, 8 caracteres).");
                    return false;
                } else {
                    if (secretOp == "") {
                        alertify.error("Ingrese la contraseña del operador.");
                        return false;
                    } else {
                        if (usuarioOp == "") {
                            alertify.error("Ingrese el usuario del operador.");
                            return false;
                        } else {
                            if (id_rol == "") {
                                alertify.error("Seleccione el rol del operador.");
                                return false;
                            }
                        }
                    }
                }
            }
        }

        datos = $('#frmaddoperador').serialize();
        console.log(datos);
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregaroperador.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmaddoperador')[0].reset();
                    $('#tabladatamantenimiento').load('tablamant.php');
                    alertify.success("Operador agregado con exito.");
                } else {
                    console.log(datos);
                    alertify.error("Fallo al agregar operador :(");
                }
            }
        });
    });
});

/* TRIGGER BOTON AGREGAR TONER_STOCK */

$(document).ready(function () {
    $('#guardartonerstock').click(function () {
        var serial_caja = $("#serial_caja").val();
        var f_ingresoStock = $("#f_ingresoStock").val();
        var contadorStock = $("#contadorStock").val();
        var cod_marcaStock = $("#cod_marcaStock").val();
        var cod_modeloStock = $("#cod_modeloStock").val();
        var status_condicionStock = $("#status_condicionStock").val();
        var observacionStock = $("#observacionStock").val();

        if (serial_caja == "") {
            alertify.error("Ingrese el serial de caja.");
            return false;
        } else {
            if (f_ingresoStock == "") {
                alertify.error("Ingrese la fecha de ingreso del toner.");
                return false;
            } else {
                if (contadorStock == "") {
                    alertify.error("Ingrese el contador del toner.");
                    return false;
                } else {
                    if (cod_marcaStock == "") {
                        alertify.error("Seleccione la marca del toner.");
                        return false;
                    } else {
                        if (cod_modeloStock == "") {
                            alertify.error("Seleccione el modelo del toner.");
                            return false;
                        } else {
                            if (status_condicionStock == "") {
                                alertify.error("Seleccione el status condicion del toner.");
                                return false;
                            } else {
                                if (observacionStock == "") {
                                    alertify.error("Ingrese la observacion del toner.");
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        datos = $('#frmaddtoner_stock').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregarstocktoner.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmaddtoner_stock')[0].reset();
                    $('#tabladatatable').load('tablaimpres.php');
                    $('#tablatoners').load('tablatoner.php');
                    alertify.success("Stock toner agregado con exito.");
                } else {
                    alertify.error("Fallo al agregar toner en stock :(");
                }
            }
        });
    });
});

/* TRIGGER BOTON AGREGAR TCHECK */
$("#refresh-btn").on("click", function () {
    $("#refresh").load("tcheck.php #refresh");
});

$(document).ready(function () {
    $('#guardartcheck').click(function () {
        var revisionIns = $("#revisionIns").val();
        var porcentajeIns = $("#porcentajeIns").val();
        var contadorIns = $("#contadorIns").val();
        var accionIns = $("#accionIns").val();
        var reporteobservacionIns = $("#reporteobservacionIns").val();

        if (revisionIns == "") {
            alertify.error("Ingrese el ID de revision.");
            return false;
        } else {
            if (porcentajeIns == "" || !porcentaje.test(porcentajeIns)) {
                alertify.error("Ingrese el % del cartucho.");
                return false;
            } else {
                if (contadorIns == "") {
                    alertify.error("Ingrese el contador del toner.");
                    return false;
                } else {
                    if (accionIns == "") {
                        alertify.error("Seleccione la accion correspondiente.");
                        return false;
                    } else {
                        if (reporteobservacionIns == "") {
                            alertify.error("Ingrese la observacion.");
                            return false;
                        } else {}
                    }
                }
            }
        }
        datos = $('#frmcheck').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/tonercheck.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmcheck')[0].reset();
                    RefreshTable();
                    alertify.success("Toner check realizado con exito.");
                } else {
                    alertify.error("Fallo al agregar toner en stock :(");
                }
            }
        });
    });
});

/* TRIGGER BOTON AGREGAR MARCA TONER */

$(document).ready(function () {
    $('#guardarmarca_toner').click(function () {
        var des_marcaToner = $("#des_marcaToner").val();

        if (des_marcaToner == "") {
            alertify.error("Ingrese la nueva marca toner.");
            return false;
        } else {}
        datos = $('#frmaddmarca_toner').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregarmarcatoner.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmaddmarca_toner')[0].reset();
                    $('#tabladatamantenimiento').load('tablamant.php');
                    alertify.success("Marca toner agregada con exito.");
                } else {
                    alertify.error("Fallo al agregar marca toner :(");
                }
            }
        });
    });
});

/* TRIGGER BOTON AGREGAR MODELO TONER */

$(document).ready(function () {
    $('#guardarmodelo_toner').click(function () {
        var des_modeloToner = $("#des_modeloToner").val();
        var id_marcaToner = $("#id_marcaToner").val();
        var colorToner = $("#colorToner").val();

        if (des_modeloToner == "") {
            alertify.error("Ingrese el serial modelo.");
            return false;
        } else {
            if (id_marcaToner == "") {
                alertify.error("Seleccione la marca del modelo toner.");
                return false;
            } else {
                if (colorToner == "") {
                    alertify.error("Ingrese el color.");
                    return false;
                } else {}
            }
        }
        datos = $('#frmaddmodelo_toner').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregarmodelotoner.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmaddmodelo_toner')[0].reset();
                    $('#tabladatamantenimiento').load('tablamant.php');
                    alertify.success("Modelo toner agregado con exito.");
                } else {
                    alertify.error("Fallo al agregar modelo toner :(");
                }
            }
        });
    });
});

/* TRIGGER BOTON AGREGAR REVISION */

$(document).ready(function () {
    $('#guardarrevisionimp').click(function () {
        var impRev = $("#impRev").val();
        var fechaRev = $("#fechaRev").val();
        var operadorRev = $("#operadorRev").val();
        var contadorRev = $("#contadorRev").val();
        var observacionRev = $("#observacionRev").val();

        if (impRev == "") {
            alertify.error("Ingrese el ID de revision.");
            return false;
        } else {
            if (fechaRev == "") {
                alertify.error("Ingrese la fecha de revision.");
                return false;
            } else {
                if (operadorRev == "") {
                    alertify.error("Cargue al operador.");
                    return false;
                } else {
                    if (contadorRev == "" || !contador.test(contadorRev)) {
                        alertify.error("Ingrese el contador, Solo numeros, max. 8 caracteres.");
                        return false;
                    } else {
                        if (observacionRev == "") {
                            alertify.error("Ingrese la observacion.");
                            return false;
                        } else {}
                    }
                }
            }
        }
        datos = $('#frmrevision').serialize();
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/agregarrevision.php",
            success: function (r) {
                if (r == 1) {
                    $('#frmrevision')[0].reset();
                    $('#tabladatamantenimiento').load('tablamant.php');                    
                    alertify.success("Revision agregada con exito.");
                } else {
                    alertify.error("Fallo al agregar Revision :(");
                }
            }
        });
    });
});

/* Asociar toner */

function asoc_toner(datosasoc) {
    dt = datosasoc.split('||');
    $('#tonerasustid').val(dt[0]);
    $('#tonerasustd').val(dt[1]);
}

function asoctonerupdate() {

    tonerasustid = $('#tonerasustid').val();
    impresoraid = $('#impresoraid').val();
    active = $('#active').val();
    serial_cartuchoStockU = $('#serial_cartuchoStockU').val();
    cadena = "tonerasustid=" + tonerasustid +
        "&impresoraid=" + impresoraid +
        "&active=" + active +
        "&serial_cartuchoStockU=" + serial_cartuchoStockU;

    console.log(cadena);

    $.ajax({
        type: 'POST',
        url: "procesos/asociartoner.php",
        data: cadena,
        success: function (r) {
            if (r == 1) {
                location.reload();
                alertify.success("Toner asociado a la impresora con exito.");

            } else {
                alertify.error("Error al asociar toner.");
            }
        }
    });
}

function cargarimpamant(datos) {
    d = datos.split('||');
    $('#id_impresoraid').val(d[0]);
    $('#id_impresoraSer').val(d[7]);
}

function rellenarformtoner(datosToner) {
    dt = datosToner.split('||');
    console.log(dt);
    $('#id_default').val(dt[0]);
    $('#serial_cartuchoStockActu').val(dt[1]);
    $('#f_ingresoStockActu').val(dt[6]);
    $('#cod_marcaStockActu').val(dt[10]);
    $('#cod_modeloStockActu').val(dt[2]);
    $('#status_condicionStockActu').val(dt[11]);
    $('#observacionStockActu').val(dt[8]);
}

function actualizadatostoner() {

    id_default = $('#id_default').val();
    serial_cartuchoStockActu = $('#serial_cartuchoStockActu').val();
    f_ingresoStockActu = $('#f_ingresoStockActu').val();
    cod_marcaStockActu = $('#cod_marcaStockActu').val();
    cod_modeloStockActu = $('#cod_modeloStockActu').val();
    status_condicionStockActu = $('#status_condicionStockActu').val();
    observacionStockActu = $('#observacionStockActu').val();

    cadena = "id_default=" + id_default +
        "&serial_cartuchoStockActu=" + serial_cartuchoStockActu +
        "&f_ingresoStockActu=" + f_ingresoStockActu +
        "&cod_marcaStockActu=" + cod_marcaStockActu +
        "&cod_modeloStockActu=" + cod_modeloStockActu +
        "&status_condicionStockActu=" + status_condicionStockActu +
        "&observacionStockActu=" + observacionStockActu;

    console.log(cadena);

    $.ajax({
        type: 'POST',
        url: "procesos/actualizartoner.php",
        data: cadena,
        success: function (r) {
            if (r == 1) {
                $('#tablatoners').load('tablatoner.php');
                alertify.success("Toner Actualizado con exito.");
            } else {
                alertify.error("Error al actualizar toner.");
            }
        }
    });
}
$(document).ready(function () {
    $('#actualizartoner').click(function () {
        actualizadatostoner();
    });
});

//Llenar imput pc
function agregapc(datospc) {
    dt = datospc.split('||');
    $('#id_pc').val(dt[0]);
    $('#nombre_equipo').val(dt[1]);
}
//Llenar imput monitor
function agregamoni(datosmoni) {
    dt = datosmoni.split('||');
    $('#id_monitor').val(dt[0]);
    $('#serial_monitor').val(dt[1]);
}
//llenar input imp
function agregaimp(datosimp) {
    dt = datosimp.split('||');
    $('#modeloimpid').val(dt[0]);
    $('#modeloimpd').val(dt[1]);
}

//caracteres usables
var expr = /^[a-zA-Z0-9_\.\-]$/;
//caracteres usables
var expr2 = /^1[8-9]|[2-5]\d|60$/;
$(document).ready(function () {
    $('#asociartoneraimpresora').click(function () {

        var tonerasustid = $("#tonerasustid").val();
        var tonerasustd = $("#tonerasustd").val();
        var serial_cartuchoStockU = $("#serial_cartuchoStockU").val();

        if (tonerasustid == "") {
            alertify.error("Seleccione el toner a asociar.");
            return false;
        }
        //en otro caso, el mensaje no se muestra
        else {


            //Si correo está vacío y la expresión NO corresponde -test es función de JQuery
            //Muestra el mensaje
            //Con false sale de los if's y espera a que sea pulsado de nuevo el botón de enviar
            if (tonerasustd == "") {

                return false;
            } else { 
                if(serial_cartuchoStockU == ""){
                    alertify.error("Introdusca el serial del cartucho a asociar.");
                    return false;
            }
                    else {
                }
            }
        }
        asoctonerupdate();
    });
});

function agregatoner(datostoner) {
    dt = datostoner.split('||');
    $('#modelotid').val(dt[0]);
    $('#modelotd').val(dt[1]);
}

/* TRAER DATOS OPERADOR A FORM REVISION */

function operadoradd(datosOp) {
    dt = datosOp.split('||');
    $('#operadorRev').val(dt[0]);
    $('#operadorNombre').val(dt[1]);
    $('#operadorApellido').val(dt[2]);
}

/* TRAER DATOS A TONER SUSTITUCION */

function agregaformtoner(datosIns) {
    dt = datosIns.split('||');
    $('#cod_tonerNew').val(dt[0]);
    $('#serial_modelotonerNew').val(dt[1]);
    $('#serial_cartuchotonerNew').val(dt[2]);
    $('#fecha_ingresotonerNew').val(dt[5]);
    $('#statusNew').val(dt[7]);
    /*$('#statusstockNew').val(dt[10]);*/
    $('#observacion_tonerNew').val(dt[6]);
}

/* TRAER DATOS DESDE TABLA A FORM ACTUALIZAR */

function tonerInsStock() {
    //Cambio de estado Ins a Stock toner
    cod_impresoraAsoc = $('#cod_impresoraAsoc').val();
    cod_tonerIns = $('#cod_tonerIns').val();
    statusstockIns = $('#statusstockIns').val();
    //Reporte
    revisionIns = $('#revisionIns').val();
    porcentajeIns = $('#porcentajeIns').val();
    contadorIns = $('#contadorIns').val();
    accionIns = $('#accionIns').val();
    reporteobservacionIns = $('#reporteobservacionIns').val();
    //Cambio de estado Stock a Ins
    cod_tonerNew = $('#cod_tonerNew').val();
    statusstockNew = $('#statusstockNew').val();

    cadena = "cod_impresoraAsoc=" + cod_impresoraAsoc +
        "&cod_tonerIns=" + cod_tonerIns +
        "&statusstockIns=" + statusstockIns +
        "&revisionIns=" + revisionIns +
        "&porcentajeIns=" + porcentajeIns +
        "&contadorIns=" + contadorIns +
        "&accionIns=" + accionIns +
        "&reporteobservacionIns=" + reporteobservacionIns +
        "&statusstockNew=" + statusstockNew +
        "&cod_tonerNew=" + cod_tonerNew +
        "&statusstockNew=" + statusstockNew;
    console.log(cadena);

    $.ajax({
        type: 'POST',
        url: "procesos/insastock.php",
        data: cadena,
        success: function (r) {
            if (r == 1) {
                // $('#tabladatatable').load('tablaimpres.php');
                alertify.success("Toner en Stock satisfactorio.");
            } else {
                alertify.error("Error en tonerInsStock.");
            }
        }
    });
}

function tonerCheck() {
    datos = $('#frmsusttoner').serialize();
    $.ajax({
        type: "POST",
        data: datos,
        url: "procesos/tonercheck.php",
        success: function (r) {
            if (r == 1) {
                alertify.success("Toner Check Exitoso.");
            } else {
                alertify.error("Fallo al agregar toner en stock :(");
            }
        }
    });
}

function tonerStockIns() {
    //Cambio de estado Ins a Stock toner
    cod_impresoraAsoc = $('#cod_impresoraAsoc').val();
    cod_tonerIns = $('#cod_tonerIns').val();
    statusstockIns = $('#statusstockIns').val();
    //Reporte
    revisionIns = $('#revisionIns').val();
    porcentajeIns = $('#porcentajeIns').val();
    contadorIns = $('#contadorIns').val();
    accionIns = $('#accionIns').val();
    reporteobservacionIns = $('#reporteobservacionIns').val();
    //Cambio de estado Stock a Ins
    cod_tonerNew = $('#cod_tonerNew').val();
    statusstockNew = $('#statusstockNew').val();

    cadena = "cod_impresoraAsoc=" + cod_impresoraAsoc +
        "&cod_tonerIns=" + cod_tonerIns +
        "&statusstockIns=" + statusstockIns +
        "&revisionIns=" + revisionIns +
        "&porcentajeIns=" + porcentajeIns +
        "&contadorIns=" + contadorIns +
        "&accionIns=" + accionIns +
        "&reporteobservacionIns=" + reporteobservacionIns +
        "&statusstockNew=" + statusstockNew +
        "&cod_tonerNew=" + cod_tonerNew +
        "&statusstockNew=" + statusstockNew;

    $.ajax({
        type: 'POST',
        url: "procesos/stockains.php",
        data: cadena,
        success: function (r) {
            if (r == 1) {
                // $('#tabladatatable').load('tablaimpres.php');
                alertify.success("Cartucho de estado stock a ins con exito.");
            } else {
                alertify.error("Error tonerStockIns.");
            }
        }
    });
}

$(document).ready(function () {
    $('#SustituirToner').click(function () {

        /* Validacion campos */

        var revisionIns = $("#revisionIns").val();
        var porcentajeIns = $("#porcentajeIns").val();
        var contadorIns = $("#contadorIns").val();
        var accionIns = $("#accionIns").val();
        var reporteobservacionIns = $("#reporteobservacionIns").val();
        var cod_tonerNew = $("#cod_tonerNew").val();

        if (revisionIns == "") {
            alertify.error("Introdusca un id revision.");
            return false;
        }
        //en otro caso, el mensaje no se muestra
        else {


            //Si correo está vacío y la expresión NO corresponde -test es función de JQuery
            //Muestra el mensaje
            //Con false sale de los if's y espera a que sea pulsado de nuevo el botón de enviar
            if (porcentajeIns == "") {
                alertify.error("Introduzca el porcentaje en reporte cartucho.");
                return false;
            } else {
                if (contadorIns == "") {
                    alertify.error("Introduzca el contador en reporte cartucho.");
                    return false;
                } else {
                    if (accionIns == "") {
                        alertify.error("Seleccione la accion en reporte cartucho.");
                        return false;
                    } else {
                        if (reporteobservacionIns == "") {
                            alertify.error("Introduzca las observaciones en reporte cartucho.");
                            return false;
                        } else {
                            if (cod_tonerNew == "") {
                                alertify.error("Seleccione el toner nuevo a instalar.");
                                return false;
                            } else {

                            }
                        }
                    }
                }
            }
        }


        tonerInsStock();
        tonerCheck();
        tonerStockIns();
    });
});

/* FUNCION ACTUALIZAR DATOS */


function agregaform(datos) {
    d = datos.split('||');
    $('#cod_impActu').val(d[0]);
    $('#des_tipoActu').val(d[1]);
    $('#des_marcaActu').val(d[3]);
    $('#des_modeloActu').val(d[5]);
    $('#serial_impActu').val(d[7]);
    $('#serialb_impActu').val(d[8]);
    $('#status_impActu').val(d[9]);
    $('#des_edificioActu').val(d[14]);
    $('#des_ubicacionActu').val(d[11]);
    $('#observacion_impActu').val(d[13]);
}

function actualizadatos() {

    cod_impActu = $('#cod_impActu').val();
    des_tipoActu = $('#des_tipoActu').val();
    des_modeloActu = $('#des_modeloActu').val();
    serial_impActu = $('#serial_impActu').val();
    serialb_impActu = $('#serialb_impActu').val();
    status_impActu = $('#status_impActu').val();
    des_ubicacionActu = $('#des_ubicacionActu').val();
    observacion_impActu = $('#observacion_impActu').val();

    cadena = "cod_impActu=" + cod_impActu +
        "&des_tipoActu=" + des_tipoActu +
        "&des_modeloActu=" + des_modeloActu +
        "&serial_impActu=" + serial_impActu +
        "&serialb_impActu=" + serialb_impActu +
        "&status_impActu=" + status_impActu +
        "&des_ubicacionActu=" + des_ubicacionActu +
        "&observacion_impActu=" + observacion_impActu;

    console.log(cadena);

    $.ajax({
        type: 'POST',
        url: "procesos/actualizarimpresora.php",
        data: cadena,
        success: function (r) {
            if (r == 1) {
                $('#tabladatatable').load('tablaimpres.php');
                alertify.success("Impresora Actualizada con exito.");
            } else {
                alertify.error("Error al actualizar impresora");
            }
        }
    });
}
$(document).ready(function () {
    $('#actualizarimpresora').click(function () {
        actualizadatos();
    });
});

/* PARTE ELIMINAR DATOS IMPRESORAS */

function segurodeeliminardatos(id) {
    alertify.confirm('Eliminar datos', '¿Esta realmente seguro de eliminar este registro?', function () {
        eliminardatos(id)
    }, function () {
        alertify.error('Se ha cancelado la operacion')
    });
}

function eliminardatos(id) {

    cadena = "id=" + id;

    $.ajax({
        type: "POST",
        url: "procesos/eliminarimpresora.php",
        data: cadena,
        success: function (r) {
            if (r == 1) {
                $('#tabladatatable').load('tablaimpres.php');
                alertify.success("Impresora eliminada con exito.");
            } else {
                alertify.error("Error al eliminar impresora.");
            }
        }
    });
}

function desocktoner(id) {
    alertify.confirm('Eliminar datos', '¿Esta realmente seguro de eliminar este registro?', function () {
        desasociartoner(id)
    }, function () {
        alertify.error('Se ha cancelado la operacion');
    });
}

function desasociartoner(id) {

    cadena = "id=" + id;

    $.ajax({
        type: "POST",
        url: "procesos/desasociar_toner.php",
        data: cadena,
        success: function (r) {
            if (r == 1) {
                alertify.success("Toner desasociado con exito.");
                location.reload();
            } else {
                alertify.error("Error al desasociar toner.");
            }
        }
    });
}

/* ELIMINAR DATOS TONERS */

function segurodeeliminartoner(id) {
    alertify.confirm('Eliminar datos', '¿Esta realmente seguro de eliminar este registro?', function () {
        eliminartoner(id)
    }, function () {
        alertify.error('Se ha cancelado la operacion');
    });
}

function eliminartoner(id) {

    cadena = "id=" + id;

    $.ajax({
        type: "POST",
        url: "procesos/eliminartoner.php",
        data: cadena,
        success: function (r) {
            if (r == 1) {
                $('#tablatoners').load('tablatoner.php');
                alertify.success("Toner eliminado con exito.");
            } else {
                alertify.error("Error al eliminar toner");
            }
        }
    });
}

//RANGE SLIDER
var rangeSlider = function () {
    var slider = $('.range-slider'),
        range = $('.range-slider__range'),
        value = $('.range-slider__value');

    slider.each(function () {

        value.each(function () {
            var value = $(this).prev().attr('value');
            $(this).html(value);
        });

        range.on('input', function () {
            $(this).next(value).html(this.value);
        });
    });
};
rangeSlider();
//DATOS SELECCIONADO EN CHECKBOX
$("#boton").click(function () {
    $("input[type=checkbox]:checked").each(function () {
        $(this).closest('td').siblings().each(function () {
            console.log($(this).text());
        });
    });
});
//sessionStorage.setItem('Serial', $('#serial').val());
//varsessionStorage.getItem('Serial');

//MODALS MULTIPLES
$(document).on('show.bs.modal', '.modal', function () {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function () {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

// LOGIN

jQuery(document).on('submit','#formlogin', function(event){
    event.preventDefault();

    jQuery.ajax({
        url: 'loginproceso.php',
        type: 'POST',
        dataType: 'json',
        data: $(this).serialize(),
        beforeSend: function(){    
            
        }
    })
    .done(function(datos){
        console.log(datos);
        if(!datos.error){
            if(datos.rol == '1'){
                location.href = 'index.php';
            }
        }else {
            alertify.error('Los datos que ha ingresado no son correctos o no existen, verifíque sus datos.');
        }
    })
    .fail(function(resp){
        console.log(resp.responseText);
    })
    .always(function(){
        console.log('Complete');
    });
});