<?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();

    $sql = "SELECT est.id_estacion, /*0*/
    est.Descripcion,                /*0*/
    p.nombre_eq,                    /*0*/
    moni.descripcion,               /*0*/
    lec.descripcion,                /*0*/
    mou.descripcion,                /*0*/
    tecla.descripcion,              /*0*/
    fis.descripcion,                /*0*/
    est.dir_ip,                     /*0*/
    dep.descripcion                 /*0*/
    FROM estacion est INNER JOIN pc p ON p.id_pc = est.id_pc INNER JOIN monitor moni ON moni.id_monitor = est.id_monitor INNER JOIN lector_barras lec ON lec.id_lector = est.id_lector INNER JOIN mouse mou ON mou.id_mouse = est.id_mouse INNER JOIN teclado tecla ON tecla.id_teclado = est.id_teclado INNER JOIN imp_fiscal fis ON fis.id_impfiscal = est.id_impfiscal INNER JOIN departamento dep ON dep.id_departamento = est.id_dpto WHERE est.id_estacion";

    $result = mysqli_query($conexion,$sql);
?>

        <div>
            <table id="estaciones" class="table table-hover custom-table">
                <thead class="thead-dark">
                    <tr class="">
                        <th scope="col">#</th>
                        <th scope="col">Estacion</th>
                        <th scope="col">Nombre de equipo</th>
                        <th scope="col">Monitor</th>
                        <th scope="col">Lector</th>
                        <th scope="col">Mouse</th>
                        <th scope="col">Teclado</th>
                        <th scope="col">Impresora Fiscal</th>
                        <th scope="col">IP</th>
                        <th scope="col">Departamento</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while ($mostrar=mysqli_fetch_row($result)){

                            $datos=$mostrar[0]."||".
                                    $mostrar[1]."||".
                                    $mostrar[2]."||".
                                    $mostrar[3]."||".
                                    $mostrar[4]."||".
                                    $mostrar[5]."||".
                                    $mostrar[6]."||".
                                    $mostrar[7]."||".
                                    $mostrar[8]."||".
                                    $mostrar[9];
                    ?>
                <tr>
                    <td><?php echo $mostrar[0] ?></td>
                    <td><?php echo $mostrar[1] ?></td>
                    <td><?php echo $mostrar[2] ?></td>
                    <td><?php echo $mostrar[3] ?></td>
                    <td><?php echo $mostrar[4] ?></td>
                    <td><?php echo $mostrar[5] ?></td>
                    <td><?php echo $mostrar[6] ?></td>
                    <td><?php echo $mostrar[7] ?></td>
                    <td><?php echo $mostrar[8] ?></td>
                    <td><?php echo $mostrar[9] ?></td>

                </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <!-- <div class="col-md-12 text-center">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tcheck">Agregar revision</button>
            <a class="btn btn-secondary" href="index.php">Volver al index</a>
        </div> -->
    </div>
    </div>

</body>
</html>

<script type="text/javascript">
    $(document).ready(function () {
        $('#estaciones').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                "infoEmpty": "Mostrando 0 to 0 of 0 Datoss",
                "infoFiltered": "(Filtrado de _MAX_ total datos)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Datos",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
    });
</script>