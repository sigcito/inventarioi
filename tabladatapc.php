<?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();

    $sql = "SELECT P.id_pc,     /*0*/
    P.nombre_eq,                /*1*/ 
    P.serial                    /*2*/ 
    FROM PC P WHERE P.id_pc";

    $result = mysqli_query($conexion,$sql);
?>

        <div>
            <table id="tablapc" class="table table-hover custom-table" data-page-length='5'>
                <thead class="thead-dark">
                    <tr class="">
                        <th scope="col">#</th>
                        <th scope="col">ID modelo</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while ($mostrar=mysqli_fetch_row($result)){

                            $datospc=$mostrar[0]."||".
                                    $mostrar[1]."||".
                                    $mostrar[2];
                    ?>
                <tr>
                    <td><?php echo $mostrar[1] ?></td>
                    <td><?php echo $mostrar[2] ?></td>
                    <td style="text-align: center;">
                        <span class="btn btn-primary btn-sm" onclick="agregapc('<?php echo $datospc;?>')"> <i class="fas fa-pencil-alt fa-lg"></i></span>
                    </td>
                </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        
<script type="text/javascript">
    $(document).ready(function () {
	$('#tablapc').DataTable({
		"language": {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
			"infoEmpty": "Mostrando 0 to 0 of 0 Datoss",
			"infoFiltered": "(Filtrado de _MAX_ total datos)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Datos",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		}
    });
});
</script>