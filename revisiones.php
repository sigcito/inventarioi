<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
    <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
    <title>APP IMPRESORAS</title>
</head>

<body>
    <!--CABECERA-->
    <div class="container">
        <h1 class="text-center mb-5 mt-4">Cartuchos Instalados</h1>
    </div>
    <!--TABLA-->
    <div class="container-fluid">
        <div class="table-responsive">
        <?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();

    $cod_toner = $conexion->real_escape_string ($_GET ['id_stock_toner']);
    $serial_modelo = $conexion->real_escape_string ($_GET ['serial_modelo']);
    $serial_cartucho = $conexion->real_escape_string ($_GET ['serial_cartucho']);
    $marca_toner = $conexion->real_escape_string ($_GET ['marca']);
    $color = $conexion->real_escape_string ($_GET ['color']);
    $fecha_ingreso = $conexion->real_escape_string ($_GET ['fechaingreso']);
    $observacion = $conexion->real_escape_string ($_GET ['observacion']);
    $status_condicion = $conexion->real_escape_string ($_GET ['status_toner']);
    $cod_impresora = $conexion->real_escape_string ($_GET ['codigoimpresora']);

    $sql = "SELECT r.id_revision, /* 0 */
    i.cod_imp,                    /* 1 */
    r.f_ejecucion,                /* 2 */
    o.nombre,                     /* 3 */
    t.contador,                   /* 4 */
    r.observaciones               /* 5 */FROM revisiones r LEFT JOIN impresora_fotocopiadora i ON r.id_impresora = i.cod_imp LEFT JOIN operadores o ON r.id_operador = o.id_operador LEFT JOIN toner_check t ON r.contador_impresora = t.contador WHERE r.id_revision=";

    $result = mysqli_query($conexion,$sql);
?>

        <div>
            <table id="revisiones" class="table table-hover custom-table">
                <thead class="thead-dark">
                    <tr class="">
                        <th scope="col">#</th>
                        <th scope="col">ID Impresora</th>
                        <th scope="col">Fecha revision</th>
                        <th scope="col">Operador</th>
                        <th scope="col">Contador</th>
                        <th scope="col">Observaciones</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while ($mostrar=mysqli_fetch_row($result)){

                          /*  $datos=$mostrar[0]."||".
                                    $mostrar[1]."||".
                                    $mostrar[2]."||".
                                    $mostrar[3]."||".
                                    $mostrar[4]."||".
                                    $mostrar[5]."||".
                                    $mostrar[6]."||".
                                    $mostrar[7]."||".
                                    $mostrar[8]."||".
                                    $mostrar[9]."||".
                                    $mostrar[10]."||".
                                    $mostrar[11]."||".
                                    $mostrar[12]."||".
                                    $mostrar[13]."||".
                                    $mostrar[14]."||".
                                    $mostrar[15];*/
                    ?>
                <tr>
                    <td><?php echo $mostrar[0] ?></td>
                    <td><?php echo $mostrar[1] ?></td>
                    <td><?php echo $mostrar[2] ?></td>
                    <td><?php echo $mostrar[3] ?></td>
                    <td><?php echo $mostrar[4] ?></td>
                    <td><?php echo $mostrar[5] ?></td>
                    <td style="text-align: center;">
                        <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modificar" onclick="agregaform('<?php echo $datos;?>')"> <i class="fas fa-pencil-alt fa-lg"></i></span>
                    </td>
                    <td style="text-align: center;">
                        <a class="btn btn-success" href="cartuchos_instalados.php">
                        <i class="fas fa-trash-alt fa-lg"></i></a>
                    </td>
                    <td style="text-align: center;">
                        <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#eliminar"><i class="fas fa-pencil-alt fa-lg"></i></span>
                    </td>
                </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-12 text-center">
            <a class="btn btn-secondary" href="index.php">Volver</a>
        </div>
    </div>
    </div>

    <!-- Modal seleccionar toner instalado-->
<div class="modal fade" id="revisiones" tabindex="-2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
            <div class="table-responsive">
                <div id="tablarevisiones"></div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

</body>
<script src="librerias/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="librerias/alertify/alertify.js"></script>
<script src="librerias/fontawesome/svg-with-js/js/fontawesome-all.js"></script>
<script src="js/index.js"></script>
<script src="js/selecttoner.js"></script>

</html>

<script type="text/javascript">
    $(document).ready(function () {
        $('#revisiones').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                "infoEmpty": "Mostrando 0 to 0 of 0 Datoss",
                "infoFiltered": "(Filtrado de _MAX_ total datos)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Datos",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    });
</script>