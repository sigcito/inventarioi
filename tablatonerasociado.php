<?php
    require_once "clases/conexion.php";
    $obj = new conectar();
    $conexion = $obj->conexion();

    $sql = "SELECT st.id_stock_toner, /*0*/
    st.serial_modelo,                 /*1*/
    st.serial_cartucho,               /*2*/
    mt.descripcion,                   /*3*/
    mat.descripcion,                  /*4*/
    mt.cod_color,                     /*5*/
    mt.color,                         /*6*/
    st.f_ingreso,                     /*7*/ 
    st.observacion,                   /*8*/
    id_status_condicion,              /*9*/  
    id_status_stock,                  /*10*/
    st.cod_imp,                       /*11*/
    st.cod_modelo,                    /*12*/
    stat.des_status_toner             /*13*/
    FROM stock_toner st LEFT JOIN modelo_toner mt ON mt.id_modelo_toner = st.cod_modelo LEFT JOIN marca_toner mat ON mat.id_marca_toner = mt.id_marca_toner LEFT JOIN status_toner stat ON stat.cod_statustoner = st.id_status_condicion WHERE id_status_stock=0";

    $result = mysqli_query($conexion,$sql);
?>

            <div>
                <table id="revisiones" class="table table-hover custom-table">
                    <thead class="thead-dark">
                        <tr class="">
                            <th scope="col">#</th>
                            <th scope="col">Serial</th>
                            <th scope="col">Serial cartucho</th>
                            <th scope="col">Modelo</th>
                            <th scope="col">Marca</th>
                            <th scope="col">Color</th>
                            <th scope="col">Status</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($mostrar=mysqli_fetch_row($result)){
                            $datosIns=$mostrar[0]."||".
                            $mostrar[1]."||".
                            $mostrar[2]."||".
                            $mostrar[3]."||".
                            $mostrar[4]."||".
                            $mostrar[5]."||".
                            $mostrar[6]."||".
                            $mostrar[7]."||".
                            $mostrar[8]."||".
                            $mostrar[9]."||".
                            $mostrar[10]."||".
                            $mostrar[11]."||".
                            $mostrar[12]."||".
                            $mostrar[13];
                    ?>
                            <tr>
                                <td>
                                    <?php echo $mostrar[0] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[1] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[2] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[3] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[4] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[5] ?>
                                </td>
                                <td>
                                    <?php echo $mostrar[13] ?>
                                </td>
                                <td style="text-align: center;">
                                    <span class="btn btn-primary btn-sm" data-dismiss="modal" onclick="agregaformtoner('<?php echo $datosIns;?>')"> <i class="fas fa-pencil-alt fa-lg"></i></span>
                                </td>
                            </tr>
                            <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
<script type="text/javascript">
    $(document).ready(function () {
	$('#revisiones').DataTable({
		"language": {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
			"infoEmpty": "Mostrando 0 to 0 of 0 Datoss",
			"infoFiltered": "(Filtrado de _MAX_ total datos)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Datos",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		}
    });
});
</script>